#!/usr/bin/python3

"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'

import os
import sys

abspath = os.path.realpath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

# HOTFIX for working PyQt5 QML, for linux only
from OpenGL import GL
from qllauncher import __version__ as qllauncher_lib_version
from qllauncher.helpers import misc
from helpers import optionsparser, __version__ as qllauncher_version
from ui.views import mainwindow
from PyQt5 import QtWidgets, QtGui, QtCore, Qt
import logging


if optionsparser.args.information:
    import sleekxmpp
    import requests
    import platform

    print('OS: %s %s %s %s' % (platform.dist(), platform.system(), platform.architecture(), platform.release()))
    print('QLLauncher library version: %s' % qllauncher_lib_version)
    print('QLLauncher application version: %s' % qllauncher_version)
    print("Qt version:", QtCore.QT_VERSION_STR)
    print("PyQt version:", Qt.PYQT_VERSION_STR)
    print('Python version: %s' % '.'.join(map(str, sys.version_info[0:3])))
    print('Python-sleekxmpp version: %s' % sleekxmpp.__version__)
    print('Python-requests version: %s' % requests.__version__)

    exit(0)

if optionsparser.args.verbosity:
    logging.basicConfig(level=(logging.INFO and logging.DEBUG),
                        format='%(levelname)-8s%(asctime)-12s%(message)s',
                        datefmt='%H:%M:%S')
else:
    logging.disable((logging.CRITICAL and logging.INFO and logging.DEBUG and logging.WARNING and logging.ERROR))

app = QtWidgets.QApplication(sys.argv)
app.setWindowIcon(QtGui.QIcon('ui/images/quakelivelogo.png'))
if sys.version_info[:1] < (3,):
    logging.error('Error, python 3 needed')

    exit(1)
elif not misc.is_connected_to_network():
    QtWidgets.QMessageBox.critical(None, 'QuakeLive Launcher Error', 'No internet connection')

    exit(2)


main_window = mainwindow.MainWindow()
sys.exit(app.exec())