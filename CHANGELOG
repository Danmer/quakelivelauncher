qllauncher 0.6.0
  * Fully rewritten user interface.
  * Added ability to see live streams.
  * Launcher now separated to python library (https://pypi.python.org/pypi?name=qllauncher&version=0.6.0&:action=display)
  which is python3-qllauncher and it's frontend - qllauncher.
  * Added friends list and chat window.
  * Added quakelive profile view.
  * Added sound notifications.

qllauncher 0.5.7
  * Fixed a bug when it was impossible to save server browser filter with "instagib" option set to "Yes" and it
  caused a crash (https://bitbucket.org/fx_/quakelivelauncher/issue/16).
  * Changed port issue when it was unable to connect if port was equal to 27000 or 28500 (https://bitbucket.org/fx_/quakelivelauncher/issue/14).
  * Added donate button in readme and interface.
  * Default sorting by player frags has been enabled in server info widget.
  * Added indication after clicking 'play' button in the main window.
  * Fixed wrong location name when server spawning with using packed settings.
  * Removed hard-coded values from server filter.
  * Added "In Your Vicinity" location (https://bitbucket.org/fx_/quakelivelauncher/issue/7).
  * Added category "Game" in the desktop file (https://bitbucket.org/fx_/quakelivelauncher/issue/9).
  * Performed user interface optimizations for faster work.
  * Added "instagib" game filter option in server filter (https://bitbucket.org/fx_/quakelivelauncher/issue/8).

qllauncher 0.5.6
  * (-i, --info) now shows system information.
  * Added coreutils as dependency.

qllauncher 0.5.5
  * Fixed wrong players count in server browser.
  * Added backward compatibility with old sleekxmpp packages (https://bitbucket.org/fx_/quakelivelauncher/issue/4).
  * Fixed some backward python compatibility issues.
  * Fixed empty server filter fields error (https://bitbucket.org/fx_/quakelivelauncher/issue/3).
  * Fixed incorrect map image layout in server browser.
  * Added missing dependencies (x11-xserver-utils).

qllauncher 0.5.4
  * Added option to skip update at startup (--skip-update).
  * Added option to show versions of modules (-i, --info).
  * Possibly fixed an issue with xep_0078 error (https://bitbucket.org/fx_/quakelivelauncher/issue/1).
  * Added wine as dependency.

qllauncher 0.5.3
  * Fixed bug when server filter did not work.

qllauncher 0.5.2
  * Game Installer implemented.

qllauncher 0.5.1
  * Coloured nickname issue fixed when nickname showed in main window was coded with ql colour codes (^1^2, etc).
  * Created workaround for known QL bug with XMPP authentication.

qllauncher 0.5.0 and earlier
  * Fixed bug issues.
  * Centered main window.
  * Corrected sorting in server info widget.
  * QuakeLive now runs much smoother and does not need awesomium at all.
  * Few UI fixes.
  * Fixed crash when user tried to start game by using public server id.
  * Fixed crash on non-pro subscriber accounts.
  * Added ability to connect by using string like "/connect 91.198.152.137:27028; password ctf".
  * Wrapped unsafe process calls.
  * Realized quakelive-updater.
  * Realized Server Browser.
  * Fixed issue when it was not possible to migrate from old config version.
  * Invites, change home/base path.