/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function getRandomInt(minNumber, maxNumber) {
    return Math.floor(Math.random() * maxNumber) + minNumber
}

function getItemIfExists(dictionary, key, notExistsItem) {
    return dictionary && (key in dictionary) ? dictionary[key] : notExistsItem
}

function filterDictByChildAttribute(dictionary, childKey, childValue) {
    var filteredDict

    if (dictionary) {
//        for (var k in dictionary) {
//            if (dictionary[key][childKey] !== value)
//        }
    }

    return filteredDict
}

function changeVisibleItemState(item, height, opacity) {
    item.height = height
    item.opacity = opacity
}

function getFSPathWithoutProtocol(path) {
    return path.replace("file://", "")
}

function vardump(obj) {
    var out = "";

    if(obj && typeof(obj) == "object"){
        for (var i in obj) {
            out += i + ": " + vardump(obj[i]) + "\n";
        }
    } else {
        out = obj;
    }

    return out
}

function isEmpty(object) {
    for(var i in object) {
        return false;
    }

    return true;
}

function stringStartsWith(string1, string2) {
    return string1.slice(0, string2.length) === string2
}

function stringEndsidth(string1, string2) {
    return string1.slice(-string2.length) === string2
}
