"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


from ui import commonqmlwindow

from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine, QQmlApplicationEngine
from ui.controllers.mainwindowbackend import MainWindowBackend
from ui.controllers.updatecheckerbackend import UpdateCheckerBackend
from ui.controllers.serverbrowserbackend import ServerBrowserBackend
from ui.controllers.friendswidgetbackend import FriendsWidgetBackend
from ui.controllers.profilewidgetbackend import ProfileWidgetBackend


class MainWindow(commonqmlwindow.CommonQMLWindow):

    #
    # use_pyqt5_workaround is a workaround for pyqt5 bug:
    #    https://bugreports.qt-project.org/browse/QTBUG-43054
    #    http://stackoverflow.com/questions/27018941/pyqt5-qml2-deploy-on-debian-mint-dev-ubuntu
    #

    def __init__(self, parent=None, use_pyqt5_workaround=True):
        file_name = 'ui/views/mainwindow.qml'
        qmlRegisterType(MainWindowBackend, 'MainWindowBackend', 1, 0, 'MainWindowBackend')
        qmlRegisterType(UpdateCheckerBackend, 'UpdateCheckerBackend', 1, 0, 'UpdateCheckerBackend')
        qmlRegisterType(ServerBrowserBackend, 'ServerBrowserBackend', 1, 0, 'ServerBrowserBackend')
        qmlRegisterType(FriendsWidgetBackend, 'FriendsWidgetBackend', 1, 0, 'FriendsWidgetBackend')
        qmlRegisterType(ProfileWidgetBackend, 'ProfileWidgetBackend', 1, 0, 'ProfileWidgetBackend')

        if use_pyqt5_workaround:
            commonqmlwindow.CommonQMLWindow.__init__(self)

            self._engine = QQmlApplicationEngine()
            self._engine.load(file_name)
        else:
            commonqmlwindow.CommonQMLWindow.__init__(self, file_name)