/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.Dialogs 1.1
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.1

import "." as Controls
import "../uihelpers.js" as UIHelpers


Item {
    id: rootItem

    height: textField.implicitHeight

    property bool pickDirectory: false
    property alias pickerTitle: picker.title
    property alias pickerSelectMask: picker.nameFilters
    property alias selectedUrl: picker.fileUrl
    property alias text: textField.text
    property alias placeholderText: textField.placeholderText


    TextField {
        id: textField

        readOnly: true

        anchors.left: parent.left
        anchors.right: searchImage.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }



    Controls.ClickableImage {
        id: searchImage

        width: parent.height
        height: parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        rotateOnHover: false
        source: "../../images/dots.png"

        onClicked: picker.open()
    }

    FileDialog {
        id: picker

        selectFolder: rootItem.pickDirectory ? true : false

        onAccepted: {
            rootItem.text = UIHelpers.getFSPathWithoutProtocol(picker.fileUrl.toString())
        }
    }
}
