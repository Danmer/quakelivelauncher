/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtGraphicalEffects 1.0

Image {
    id: rootItem

    property bool isPressed: false
    property real defaultOpacity: 0.3
    property real focusOpacity: 1
    property bool hovered: false
    property bool rotateOnHover: true
    property bool bounceOnClicked: false
    property real bounceOnClickedInterval: 1000
    property bool isBouncing: false
    property bool helpTextOnHover: true
    property alias helpText: helpTextItem.text
    property bool helpTextShowOnChange: true
    property alias helpTextColor: helpTextItem.color
    property alias helpTextFont: helpTextItem.font
    property alias helpTextHideInterval: helpTextHideTimer.interval
    visible: height && width ? true : false
    signal clicked()
    horizontalAlignment: Text.AlignHCenter
    opacity: hovered || privateProperties.helpTextShow ? focusOpacity : defaultOpacity

    /* Private property */
    QtObject {
        id: privateProperties

        property bool helpTextShow: !helpTextOnHover
        property bool bounceOnClickState: false
    }

    onIsBouncingChanged: {
        if (!isBouncing) {
            rootItem.scale = 1
        }
    }

    onHelpTextChanged: {
        privateProperties.helpTextShow = helpTextShowOnChange ? true : false

        helpTextHideTimer.restart()
    }

    onClicked: {
        if (rootItem.bounceOnClicked) {
            privateProperties.bounceOnClickState = true
        }
    }

    Item {
        id: helpItem

        anchors.left: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: helpText.toString().length > 0 && (rootItem.hovered && helpTextOnHover || privateProperties.helpTextShow) ? helpTextItem.paintedHeight : 0
        opacity: helpText.toString().length > 0 && (rootItem.hovered && helpTextOnHover || privateProperties.helpTextShow) ? 1 : 0

        Behavior on opacity {
            NumberAnimation { duration: 1450
                easing {
                    type: Easing.OutElastic
                    amplitude: 2.5
                    period: 2.5
                }
            }
        }

        Behavior on height {
            NumberAnimation { duration: 450
                easing {
                    type: Easing.OutElastic
                    amplitude: 2.5
                    period: 2.5
                }
            }
        }

        Text {
            id: helpTextItem

            smooth: true

            Timer {
                id: helpTextHideTimer

                running: (helpText.toString().length > 0 && privateProperties.helpTextShow) ? true : false
                interval: 3500

                onTriggered: privateProperties.helpTextShow = false
            }

            Timer {
                id: bounceOnClickedTimer

                running: privateProperties.bounceOnClickState
                interval: rootItem.bounceOnClickedInterval

                onRunningChanged: rootItem.isBouncing = running

                onTriggered: privateProperties.bounceOnClickState = false
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor

        onClicked: rootItem.clicked()
        onPressed: rootItem.isPressed = true
        onReleased: rootItem.isPressed = false
        onEntered: rootItem.hovered = true
        onExited: rootItem.hovered = false
    }


    SequentialAnimation {
        id: rotationAnimation

        running: hovered && rotateOnHover ? true : false

        loops: Animation.Infinite
        NumberAnimation { target: rootItem; property: "rotation"; duration: 2000; easing.type: Easing.InOutQuad; from: 0; to: 360 }
    }

    SequentialAnimation {
        running: isBouncing

        loops: Animation.Infinite
        NumberAnimation { target: rootItem; property: "scale"; duration: 200; easing.type: Easing.InOutQuad; from: 1; to: 0.85 }
        NumberAnimation { target: rootItem; property: "scale"; duration: 200; easing.type: Easing.InOutQuad; from: 0.85; to: 1.10 }
        NumberAnimation { target: rootItem; property: "scale"; duration: 200; easing.type: Easing.InOutQuad; from: 1.10; to: 1 }
        NumberAnimation { target: rootItem; property: "scale"; duration: 200; easing.type: Easing.InOutQuad; from: 1; to: 0.9 }
        NumberAnimation { target: rootItem; property: "scale"; duration: 200; easing.type: Easing.InOutQuad; from: 0.9; to: 1 }
    }
}
