import QtQuick 2.2

Item {
    MouseArea {
        anchors.fill: parent

        onClicked: contentItem.forceActiveFocus()
    }
}
