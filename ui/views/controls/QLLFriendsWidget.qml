/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.1
import QtGraphicalEffects 1.0

import fx.qml.controls 1.0 as FX

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SideWidget {
    id: rootItem

    titleText: "Friends"
    // friends widget properties
    property QtObject friendsWidgetBackend
    signal openServerRequested(string serverId)
    signal profileOpenRequested(string name)
    property var currentlyOnServer: friendsWidgetBackend.owner_currently_on_server


    content: Row {
        anchors.fill: parent
        spacing: 15

        ListView {
            id: friendsListView

            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 250

            model: friendsWidgetBackend.friends_model
            delegate: Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 32

                color: {
                    if (itemMouseArea.hovered || friendsListView.currentIndex === index) {
                        return "lightblue"
                    } else if (modelData.has_unread_messages) {
                        return "#fce29b"
                    } else {
                        return "#ddd"
                    }
                }

                ClickableImage {
                    id: profileImage
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: height
                    rotateOnHover: false
                    z: 2

                    source: "../../images/profile.png"

                    onClicked: profileOpenRequested(modelData.user)
                }

                FX.Marquee {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: profileImage.right
                    anchors.right: inviteImage.left
                    text: modelData.user
                    font.pointSize: 13
                }

                ClickableImage {
                    id: inviteImage
                    height: parent.height
                    width: height
                    rotateOnHover: false
                    anchors.right: gameButton.left
                    anchors.verticalCenter: parent.verticalCenter
                    visible: currentlyOnServer ? true : false

                    z: 2

                    source: "../../images/plus.png"

                    onClicked: {
                        friendsWidgetBackend.invite_user_to_current_server(modelData.user)

                        messageDialog.title = "Invite status"
                        messageDialog.text = "An invite has been sent to <b>" + modelData.user + "</b>"
                        messageDialog.icon = StandardIcon.Information
                        messageDialog.open()
                    }
                }

                ClickableImage {
                    id: gameButton

                    height: parent.height
                    width: height
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    rotateOnHover: false
                    visible: modelData.current_server ? true : false
                    z: 2

                    source: "../../images/quakelivelogo.png"

                    onClicked: {
                        if (modelData.current_server) {
                            rootItem.openServerRequested(modelData.current_server["SERVER_ID"])
                        }
                    }
                }

                MouseArea {
                    id: itemMouseArea
                    anchors.fill: parent
                    property bool hovered: false
                    propagateComposedEvents: true
                    hoverEnabled: true
                    onEntered: hovered = true
                    onExited: hovered = false
                    onClicked: { friendsWidgetBackend.message_companion = modelData; friendsListView.currentIndex = index; mouse.accepted = false }
                }
            }

            onCountChanged: findCurrentCompanion()

            function findCurrentCompanion() {
                if (friendsWidgetBackend.message_companion) {
                    var i = -1

                    for (var j = 0; j < count; j++) {
                        if (friendsWidgetBackend.friends_model[j].user === friendsWidgetBackend.message_companion.user) {
                            i = j
                            break
                        }
                    }

                    friendsListView.currentIndex = i
                    if (i === -1) {
                        friendsWidgetBackend.message_companion = undefined
                    }
                }
            }
        }

        Column {
            visible: friendsWidgetBackend.message_companion ? true : false
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - friendsListView.width
            enabled: currentlyOnServer ? false : true

            TextArea {
                id: chatViewArea

                property string messages: friendsWidgetBackend.current_messages
                readOnly: true
                height: parent.height - sendArea.height
                width: parent.width

                textFormat: Text.RichText
                wrapMode: TextEdit.Wrap

                onMessagesChanged: {
                    text = ""
                    append(messages)
                }
            }

            TextField {
                id: sendArea
                placeholderText: {
                    if (currentlyOnServer) {
                        return "Can't message while in game"
                    } else if (friendsWidgetBackend.message_companion) {
                        return "Enter a message to <b>" + friendsWidgetBackend.message_companion.user +"</b>"
                    } else {
                        return ""
                    }
                }

                width: parent.width

                Keys.onReturnPressed: sendMessage()
                Keys.onEnterPressed: sendMessage()

                function sendMessage() {
                    var trimmedText = text.trim()
                    if (trimmedText.length) {
                        friendsWidgetBackend.send_message(friendsWidgetBackend.message_companion, trimmedText)
                        text = ""
                    }
                }
            }
        }
    }

    MessageDialog {
        id: messageDialog
    }
}
