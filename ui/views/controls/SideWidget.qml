/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import "." as QLL
import "../uihelpers.js" as UIHelpers


Item {
    id: rootItem

    width: isVisible ? maxWidth : 0
    opacity: isVisible ? 1 : 0
    visible: width && height

    /* Properties */
    property int maxWidth: 670
    property bool isVisible: false
    property alias titleText: title.text
    property alias descriptionText: descriptionText.text
    property alias color: backgroundRectangle.color
    property int widthTransitionDuration: 450
    property int opacityTransitionDuration: 450
    property alias descriptionDelegate: descriptionItemWrapper.children
    property bool showExitButton: true
    property alias content: contentItem.children

    /* Signals */
    signal widgetClosing()

    onWidgetClosing: isVisible = false

    onIsVisibleChanged: {
        if (!isVisible) {
            widgetClosing()
        }
    }

    QLL.FocusedItem {
        id: widgetContentItem
        anchors.fill: parent


        Rectangle {
            id: backgroundRectangle

            anchors.fill: parent
            color: "lightgrey"
        }

        Text {
            id: title

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.topMargin: 15

            font.pointSize: 22

            visible: rootItem.titleText.length ? true : false
            height: visible ? implicitHeight : 0
        }

        QLL.ClickableImage {
            id: exitButton

            width: 32
            height: 32
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 15
            anchors.topMargin: 15

            visible: rootItem.showExitButton

            source: "../../images/close.png"

            rotateOnHover: false

            onClicked: widgetClosing()
        }

        Item {
            id: descriptionItemWrapper

            anchors.top: title.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            visible: true
            height: visible ? childrenRect.height : 0

            Text {
                id: descriptionText

                anchors.horizontalCenter: parent.horizontalCenter

                font.pointSize: 15

                visible: rootItem.descriptionText.length ? true : false
                height: visible ? implicitHeight : 0
            }
        }

        Item {
            id: contentItem

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: descriptionItemWrapper.bottom
            anchors.bottom: parent.bottom
            anchors.margins: 20
        }
    }
}
