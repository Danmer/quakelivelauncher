/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import fx.qml.helpers 1.0
import fx.qml.controls 1.0

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SideWidget {
    id: rootItem


    /* Properties */
    property var widgetData: null
    maxWidth: 192
    showExitButton: false

    /* Signals */
    signal joinButtonClicked()

    Item {
        anchors.fill: parent

        Rectangle {
            id: serverName

            height: 32

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top

            color: "#e3e3e3"

            Marquee {
                anchors.left: parent.left
                anchors.right: resetButton.left
                anchors.verticalCenter: parent.verticalCenter

                text: UIHelpers.getItemIfExists(widgetData, "host_name", "Undefined server")
                font.pointSize: 15
            }

            QLL.ClickableImage {
                id: resetButton

                width: 32
                height: 32
                anchors.right: parent.right
                anchors.top: parent.top

                source: "../../images/close.png"

                rotateOnHover: false

                onClicked: { widgetData = null; widgetClosing() }
            }
        }

        Image {
            id: mapImage
            height: 128

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: serverName.bottom

            source: widgetData && widgetData.map ? userInfo.homePath + "/.qllauncher/levelshots/" + widgetData.map + ".jpg" : "../../images/map_undefined.jpg"
            asynchronous: true

            QLL.ClickableText {
                text: "Click to join"
                anchors.centerIn: parent

                onClicked: joinButtonClicked()
            }
        }

        QLL.QLLTabWidget {
            id: tabWidget

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: mapImage.bottom
            anchors.bottom: parent.bottom

            Rectangle {
                property string title: "Players"
                anchors.fill: parent
                color: "#e3e3e3"

                TableView {
                    anchors.fill: parent
                    model: UIHelpers.getItemIfExists(widgetData, "players", [])
                    onRowCountChanged: resizeColumnsToContents()
                    sortIndicatorVisible: true
                    sortIndicatorOrder: Qt.DescendingOrder

                    TableViewColumn { role: "team" ; title: "Team"; width:40 }
                    TableViewColumn { role: "name"  ; title: "Name"; width: 100 }
                    TableViewColumn { role: "score"  ; title: "Score"; width: 40 }
                }
            }

            Rectangle {
                property string title: "Server"
                anchors.fill: parent
                color: "#e3e3e3"

                TableView {
                    model: [widgetData]
                    onRowCountChanged: resizeColumnsToContents()

                    TableViewColumn { role: "host_address"; title: "IP"; }
                }
            }
        }
    }


    UserInfo {
        id: userInfo
    }
}
