import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import "../controls/" as QL
import "../uihelpers.js" as UIHelpers

QL.SideWidget {
    id: rootItem

    /* Properties */
    property string streamTag: "quake"
    property int streamLimit: 10
    property int streamCacheLimit: 10
    property var model
    property QtObject mainWindowBackend


    /* Childs */
    onActiveFocusChanged: if (activeFocus) { mainWindowBackend.update_live_streams(streamTag, streamLimit) }

    titleText: "Live Streams"
    descriptionText: "Most recent <strong>" + rootItem.model.length + "</strong> live streams tagged \"<strong>" + rootItem.streamTag + "</strong>\""


    content: Item {
        anchors.fill: parent

        BusyIndicator {
            anchors.centerIn: parent
            id: checkingUpdateBusyIndicator
            running: visible && mainWindowBackend.streams_is_updating ? true : false
            opacity: visible && mainWindowBackend.streams_is_updating ? 1 : 0

            Behavior on opacity {
                NumberAnimation { duration: 1450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }

        Text {
            anchors.centerIn: parent

            text: "No live streams found"
            clip: true
            font.pointSize: 16
            height: visible && !mainWindowBackend.streams_is_updating && rootItem.model && !rootItem.model.length ? implicitHeight : 0

            Behavior on height {
                NumberAnimation { duration: 1450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }

        ScrollView {
            id: streamsScrollView

            anchors.fill: parent
            anchors.margins: 20

            clip:true

            contentItem: ListView {
                model: rootItem.model
                delegate: Component {
                    Column {
                        anchors.left: parent.left

                        width: 320
                        height: 200

                        Row {
                            spacing: 5

                            Image {
                                id: previewImage

                                width: 320
                                height: 160

                                source: modelData.preview_image


                                QL.ClickableImage {
                                    anchors.centerIn: parent

                                    rotateOnHover: false
                                    visible: previewImage.progress === 1.0
                                    defaultOpacity: 0.6
                                    height: 48
                                    width: 48

                                    source: "../../images/view.png"

                                    ColorOverlay {
                                        anchors.fill: parent
                                        source: parent
                                        color: "white"
                                    }

                                    onClicked: Qt.openUrlExternally(modelData.url)
                                }

                                BusyIndicator {
                                    anchors.centerIn: parent
                                    id: previewLoadingIndicator
                                    running: previewImage.progress !== 1.0 ? true : false
                                    opacity: previewImage.progress !== 1.0 ? 1 : 0

                                    Behavior on opacity {
                                        NumberAnimation { duration: 1450
                                            easing {
                                                type: Easing.OutElastic
                                                amplitude: 2.5
                                                period: 2.5
                                            }
                                        }
                                    }
                                }
                            }

                            Column {
                                Text {
                                    text: modelData.name

                                    font.pointSize: 16

                                    wrapMode: Text.Wrap
                                }

                                Row {
                                    spacing: 5

                                    Image {
                                        id: viewersImage

                                        height: 32
                                        width: 32

                                        source: "../../images/view.png"
                                    }

                                    Text {
                                        text: modelData.viewers
                                        anchors.verticalCenter: viewersImage.verticalCenter

                                        font.pointSize: 16
                                    }
                                }
                            }
                        }
                    }
                }

                populate: Transition {
                    NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 750 }
                }
            }
        }
    }

    Timer {
        interval: 30000
        running: visible
        repeat: true

        onTriggered: mainWindowBackend.update_live_streams(streamTag, streamLimit)
    }
}
