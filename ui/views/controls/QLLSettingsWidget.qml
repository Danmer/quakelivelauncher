/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtMultimedia 5.0

import fx.qml.controls 1.0 as FX
import fx.qml.helpers 1.0 as FXH

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SideWidget {
    id: rootItem

    // settings properties
    property var settingsDictionary
    property var updatedSettingsDictionary


    titleText: "Settings"

    content: Column {
        anchors.fill: parent
        spacing: 5


        QLL.FSItemPicker {
            id: wineSkinPathPicker
            anchors.left: parent.left
            anchors.right: parent.right
            visible: sysInfo.distribution.platform === "Darwin"

            placeholderText: "QuakeLive Wineskin Wrapper Path"
            pickerSelectMask: "Wineskin Wrapper (*.app)"
            pickerTitle: placeholderText
            text: UIHelpers.getItemIfExists(settingsDictionary, "wineskin_path", "")

            onTextChanged: settingsDictionary["wineskin_path"] = text
        }

        QLL.FSItemPicker {
            id: binaryPathPicker
            anchors.left: parent.left
            anchors.right: parent.right

            placeholderText: "QuakeLive Binary Path"
            pickerSelectMask: "QuakeLive Binary (quakelive.exe)"
            pickerTitle: placeholderText
            text: UIHelpers.getItemIfExists(settingsDictionary, "quake_live_binary_path", "")

            onTextChanged: settingsDictionary["quake_live_binary_path"] = text
        }

        QLL.FSItemPicker {
            id: binaryBasePicker
            anchors.left: parent.left
            anchors.right: parent.right

            pickDirectory: true
            placeholderText: "QuakeLive Base Path"
            pickerTitle: placeholderText
            text: UIHelpers.getItemIfExists(settingsDictionary, "quake_live_base_path", "")

            onTextChanged: settingsDictionary["quake_live_base_path"] = text
        }

        QLL.FSItemPicker {
            id: binaryHomePicker
            anchors.left: parent.left
            anchors.right: parent.right

            pickDirectory: true
            placeholderText: "QuakeLive Home Path"
            pickerTitle: placeholderText
            text: UIHelpers.getItemIfExists(settingsDictionary, "quake_live_home_path", "")

            onTextChanged: settingsDictionary["quake_live_home_path"] = text
        }

        TextField {
            id: emailTextField
            anchors.left: parent.left
            anchors.right: parent.right
            placeholderText: "Email"
            text: UIHelpers.getItemIfExists(settingsDictionary, "email", "")

            onTextChanged: settingsDictionary["email"] = text
        }

        TextField {
            id: passwordTextField
            anchors.left: parent.left
            anchors.right: parent.right
            placeholderText: "Password"
            echoMode: TextInput.Password
            text: UIHelpers.getItemIfExists(settingsDictionary, "password", "")

            onTextChanged: settingsDictionary["password"] = text
        }

        Column {
            spacing: 5

            Row {
                spacing: 20

                GroupBox {
                    title: "Miscellaneous"

                    Column {
                        anchors.fill: parent
                        spacing: 5

                        FX.Switch {
                            id: singleCoreCheckBox
                            onText: "Single Core"
                            offText: "Multiple Core"
                            onColor: "gray"
                            offColor: onColor
                            width: 120
                            height: 24
                            switched: UIHelpers.getItemIfExists(settingsDictionary, "taskset", false)

                            onSwitchedChanged: settingsDictionary["taskset"] = switched
                        }

                        FX.Switch {
                            id: accelerationCheckBox
                            onText: "Acceleration"
                            offText: "Acceleration"
                            offColor: "#ff8870"
                            onColor: "#70FF85"
                            width: 120
                            height: 24
                            switched: UIHelpers.getItemIfExists(settingsDictionary, "acceleration", "True") === "True"

                            onSwitchedChanged: {
                                settingsDictionary["acceleration"] = switched

                                applyAcceleration()
                            }

                            Component.onCompleted: {
                                applyAcceleration()
                            }

                            function applyAcceleration() {
                                if (switched) {
                                    appLauncher.unsafeCall("xset m 1 1", true)
                                } else {
                                    appLauncher.unsafeCall("xset m 0 0", true)
                                }
                            }
                        }
                    }
                }

                GroupBox {
                    title: "Audio"

                    Column {
                        anchors.fill: parent

                        Row {
                            spacing: 5

                            StatedClickableImage {
                                anchors.verticalCenter: parent.verticalCenter
                                id: volumeButton
                                height: 24
                                width: 24
                                rotateOnHover: false

                                state: UIHelpers.getItemIfExists(settingsDictionary, "sound_muted", "False") === "True" ? "on" : "off"
                                property bool isMuted: state === "on"

                                onIsMutedChanged: {
                                    settingsDictionary["sound_muted"] = isMuted
                                    updateAudioState()
                                }
                            }

                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                text: "Volume: " + (volumeButton.isMuted ? "muted" : Number(volumeSlider.value).toFixed(0) + "%")
                            }
                        }

                        Slider {
                            id: volumeSlider
                            enabled: !volumeButton.isMuted
                            stepSize: 5.0
                            maximumValue: 100.0
                            minimumValue: 5.0
                            updateValueWhileDragging: false
                            value: UIHelpers.getItemIfExists(settingsDictionary, "sound_volume", 5)

                            onValueChanged: {
                                if (settingsDictionary) {
                                    settingsDictionary["sound_volume"] = value
                                }
                                updateAudioState()
                            }
                        }

                        Audio {
                            id: audioSystem

                            loops: 1
                            muted: volumeButton.isMuted || !rootItem.visible
                            volume: volumeSlider.value / 100

                            function playSound(soundSource) {
                                this.stop()
                                this.source = soundSource
                                this.play()
                            }
                        }
                    }
                }
            }
        }
    }

    descriptionDelegate: Column {
        spacing: 5
        anchors.horizontalCenter: parent.horizontalCenter

        Row {
            spacing: 5

            Image {
                height: parent.height
                width: height
                sourceSize.width: width
                sourceSize.height: height

                source: {
                    if (sysInfo.distribution.platform === "Darwin") {
                        return "../../images/apple.svg"
                    } else if (sysInfo.distribution.platform === "Windows") {
                        return "../../images/windows.svg"
                    } else {
                        return "../../images/linux.svg"
                    }
                }
            }

            Column {
                spacing: 2

                Text {
                    text: sysInfo.distribution.architecture + " bit"
                }

                Text {
                    text: sysInfo.distribution.release
                }

                Text {
                    text: sysInfo.distribution.version
                }
            }
        }
    }


    function updateAudioState() {
        if (volumeButton.isMuted) {
            volumeButton.source = "../../images/volume_muted.png"
        } else {
            if (volumeSlider.value <= 33) {
                volumeButton.source = "../../images/volume_low.png"
            } else if (volumeSlider.value > 33 && volumeSlider.value <= 66) {
                volumeButton.source = "../../images/volume_mid.png"
            } else {
                volumeButton.source = "../../images/volume_high.png"
            }
        }

        if (audioSystem) {
            audioSystem.playSound("../../../sounds/notification.mp3")
        }
    }

    FXH.SysInfo {
        id: sysInfo
    }

    FXH.AppLauncher {
        id: appLauncher
    }
}
