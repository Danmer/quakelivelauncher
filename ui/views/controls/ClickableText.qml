/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2

Text {
    id: root

    property bool isPressed: false
    property color defaultColor: "#ccc"
    property color focusColor: "#777"
    property bool effectsOnFocus: true
    property bool hovered: false
    property int pressedSize: 19
    property int defaultSize: 20
    signal clicked()
    horizontalAlignment: Text.AlignHCenter
    font.pointSize: isPressed ? pressedSize : defaultSize
    color: hovered ? focusColor : defaultColor

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor

        onClicked: root.clicked()
        onPressed: root.isPressed = true
        onReleased: root.isPressed = false
        onEntered: root.hovered = true
        onExited: root.hovered = false
    }
}
