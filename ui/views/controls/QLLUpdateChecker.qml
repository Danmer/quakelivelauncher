/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.1

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SystemSideWidget {
    id: rootItem


    /* Properties */
    property QtObject updateCheckerBackend
    isVisible: updateCheckerBackend.update_state !== 0 && updateCheckerBackend.update_state !== 4

    /* Signals */
    signal specifyExisting()


    titleText: "Checking Updates"

    Component.onCompleted: {
        if (updateCheckerBackend.startup_update_check) {
            updateCheckerBackend.check_update()
        }
    }

    content: Item {
        anchors.fill: parent

        Column {
            id: checkingUpdateDialog

            anchors.centerIn: parent
            spacing: 25
            visible: updateCheckerBackend.update_state === 1 | updateCheckerBackend.update_state === 3 ? true : false
            height: visible ? implicitHeight : 0
            opacity: visible ? 1 : 0

            Item {
                BusyIndicator {
                    anchors.centerIn: parent
                    id: checkingUpdateBusyIndicator
                    running: true
                }

                Text {
                    id: updateText
                    anchors.top: checkingUpdateBusyIndicator.bottom
                    anchors.topMargin: 15
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: updateCheckerBackend.update_text
                }
            }


            Behavior on opacity {
                NumberAnimation { duration: 450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }

            Behavior on height {
                NumberAnimation { duration: 450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }

        Column {
            id: noGameFoundDialog

            anchors.centerIn: parent
            spacing: 25
            visible: updateCheckerBackend.update_state === 2 ? true : false
            height: visible ? implicitHeight : 0
            opacity: visible ? 1 : 0

            Text {
                text: "No QuakeLive found on your computer"
                font.pointSize: 12
            }

            Row {
                spacing: 25

                Button {
                    text: "Download and Install"
                    onClicked: {
                        installDirectoryPicker.open()
                    }
                }

                Button {
                    text: "Specify Existing"
                    onClicked: rootItem.specifyExisting()
                }
            }

            Behavior on opacity {
                NumberAnimation { duration: 450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }

            Behavior on height {
                NumberAnimation { duration: 450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }


        FileDialog {
            id: installDirectoryPicker

            selectFolder: true
            title: "Select QuakeLive Installation Directory"

            onAccepted: {
                updateCheckerBackend.installGameTo(UIHelpers.getFSPathWithoutProtocol(fileUrl.toString()))
            }
        }
    }
}
