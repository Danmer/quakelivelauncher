/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import fx.qml.controls 1.0

import "." as QL

Item {
    id: rootItem


    /* Properties */
//    property Item openWidgetItem: openWidget
    property var myServers
//    readonly property int maxMenuHeight: openWidget.height + browserButton.height + friendsButton.height
    readonly property int maxMenuHeight: column.implicitHeight

    /* Signals */
    signal serverOpenRequested(string serverString)
    signal browserRequested()
    signal friendsRequested()
    signal profileRequested()

    property alias showFriendsNotification: friendsButton.showMark


    Column {
        id: column

        anchors.fill: parent

        QL.ClickableImage {
            id: browserButton

            height: 48
            width: 48

            rotateOnHover: false
            source: "../../images/list.png"

            onClicked: {
                browserRequested()
            }

            ColorOverlay {
                anchors.fill: parent
                source: parent
                color: "#ddd"
            }
        }

        NotificationItem {
            id: friendsButton
            width: 48
            height: 48

            markBackgroundColor: "steelblue"

            QL.ClickableImage {
                anchors.fill: parent

                rotateOnHover: false
                source: "../../images/people.png"

                onClicked: {
                    friendsRequested()
                }

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#ddd"
                }
            }
        }

        QL.ClickableImage {
            id: profileButton

            height: 48
            width: 48

            rotateOnHover: false
            source: "../../images/profile.png"

            onClicked: {
                profileRequested()
            }

            ColorOverlay {
                anchors.fill: parent
                source: parent
                color: "#ddd"
            }
        }
    }
}
