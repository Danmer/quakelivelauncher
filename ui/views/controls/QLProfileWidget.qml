/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SideWidget {
    id: rootItem

    titleText: "Profile"
    // profile widget properties
    property QtObject profileWidgetBackend


    content: Item {
        anchors.fill: parent

        Row {
            anchors.fill: parent
            spacing: 150
            opacity: profileWidgetBackend.profile_fetch_status !== 3 ? 0 : 1

            Image {
                source: UIHelpers.getItemIfExists(profileWidgetBackend.profile_data, "model", "")
            }

            Column {
                Row {
                    spacing: 15

                    Image {
                        anchors.verticalCenter: parent.verticalCenter
                        source: UIHelpers.getItemIfExists(profileWidgetBackend.profile_data, "flag", "")
                    }

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        text: UIHelpers.getItemIfExists(profileWidgetBackend.profile_data, "name", "")
                        font.pointSize: 18
                    }
                }

                Text {
                    text: UIHelpers.getItemIfExists(profileWidgetBackend.profile_data, "summary", "")
                    font.pointSize: 12
                }
            }

            Behavior on opacity {
                NumberAnimation { duration: 1450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }

        Item {
            anchors.fill: parent
            opacity: profileWidgetBackend.profile_fetch_status !== 3 ? 1 : 0

            Column {
                anchors.centerIn: parent
                spacing: 10

                BusyIndicator {
                    anchors.horizontalCenter: parent.horizontalCenter
                    running: profileWidgetBackend.profile_fetch_status === 1
                    height: profileWidgetBackend.profile_fetch_status !== 3 ? implicitHeight : 0

                    Behavior on height {
                        NumberAnimation { duration: 1450
                            easing {
                                type: Easing.OutElastic
                                amplitude: 2.5
                                period: 2.5
                            }
                        }
                    }
                }

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: {
                        if (profileWidgetBackend.profile_fetch_status === 2) {
                            return "Profile <b>" + profileWidgetBackend.target_profile + "</b> dont seem to be exists"
                        } else if (profileWidgetBackend.profile_fetch_status === 1) {
                            return "Fetching profile information for: <b>" + profileWidgetBackend.target_profile + "</b>"
                        } else {
                            return "Nothing to do"
                        }

                        // the following does not - WHY?
//                        if (profileWidgetBackend.profile_fetch_status === 1) {
//                            return "Fetching profile information for: <b>" + profileWidgetBackend.target_profile + "</b>"
//                        } else if (profileWidgetBackend.profile_fetch_status === 2) {
//                            return "Profile <b>" + profileWidgetBackend.target_profile + "</b> dont seem to be exists"
//                        } else {
//                            return "Nothing to do"
//                        }
                    }
                    font.pointSize: 13
                    clip: true
                }
            }

            Behavior on opacity {
                NumberAnimation { duration: 1450
                    easing {
                        type: Easing.OutElastic
                        amplitude: 2.5
                        period: 2.5
                    }
                }
            }
        }
    }
}
