/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2


Item {
    id: rootItem


    property alias title: dockTitle.text
    property bool fromRight: false
    property bool isVisible: false
    property int minimumWidth: dockTitleRectangle.width
    property int minimumHeight: 260
    property alias dockContentItem: dockContent.children

//    width: width < minimumWidth ? minimumWidth : width
//    height: height < minimumHeight ? minimumHeight : height
    width: isVisible ? dockTitleRectangle.width + dockContent.width : minimumWidth
    height: minimumHeight

    onHeightChanged: {
        if (height < minimumHeight) {
            height = minimumHeight
        }
    }

    onWidthChanged: {
        if (width < minimumWidth) {
            width = minimumWidth
        }
    }


    onIsVisibleChanged: {
        console.log('widths: ' + dockTitleRectangle.width + " || " + dockContent.width)
    }

    Item {
        id: contentItem

        anchors.fill: parent

        clip: true

        Row {
            Rectangle {
                id: dockTitleRectangle

                color: "#bbb"

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: rootItem.fromRight ? parent.left : undefined
                anchors.right: !rootItem.fromRight ? parent.right : undefined

                width: dockTitle.implicitWidth
                height: dockTitle.implicitHeight

                Text {
                    id: dockTitle

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter

                    rotation: -90
                }

                MouseArea {
                    anchors.fill: parent

                    onClicked: rootItem.isVisible = !rootItem.isVisible
                }
            }

            Item {
                id: dockContent

                clip: true

                width: rootItem.isVisible ? dockTitleRectangle.width + childrenRect.width : dockTitleRectangle.width
                height: childrenRect.height

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: rootItem.fromRight ? dockTitleRectangle.right : undefined
                anchors.right: !rootItem.fromRight ? dockTitleRectangle.left : undefined
            }
        }
    }

//    Item {
//        id: contentItem

//        anchors.fill: parent

//        clip: true

//        Rectangle {
//            id: dockRectangle

//            anchors.fill: parent

//            color: "#bbb"

//            Text {
//                id: dockTitle

//                anchors.fill: parent
//                anchors.verticalCenter: parent.verticalCenter

//                rotation: -90
//            }

//            MouseArea {
//                anchors.fill: parent

//                onClicked: isVisible = !isVisible
//            }
//        }

//        Item {
//            id: dockContent

//            clip: true

//            width: rootItem.isVisible ? dockRectangle.width + childrenRect.width : dockRectangle.width
//            height: childrenRect.height

//            anchors.top: parent.top
//            anchors.bottom: parent.bottom
//            anchors.left: rootItem.fromRight ? dockRectangle.right : undefined
//            anchors.right: !rootItem.fromRight ? dockRectangle.left : undefined
//        }
//    }

    Behavior on width {
        NumberAnimation { duration: 750
            easing {
                type: Easing.OutElastic
                amplitude: 2.5
                period: 2.5
            }
        }
    }
}
