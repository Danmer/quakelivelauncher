/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1

import fx.qml.controls 1.0

import "." as QLL
import "../uihelpers.js" as UIHelpers


QLL.SideWidget {
    id: rootItem

    titleText: "Server Browser"

    // serverbrowser properties
    property QtObject serverBrowserBackend
    property QtObject mainWindowBackend
    property variant servers: serverBrowserBackend.servers
    property var filters: serverBrowserBackend.filters
    property var spawnParameters: serverBrowserBackend.spawn_parameters


    Connections {
        target: serverBrowserBackend

        onSpawnedServerLinksChanged: {
            if (serverBrowserBackend.spawned_server_links) {
                messageDialog.title = "Your server links"
                messageDialog.text =
                        "<b>Public id</b>: http://quakelive.com/#!join/" + serverBrowserBackend.spawned_server_links.public_id + "<br/>" +
                        "<b>Invitation code</b>: http://quakelive.com/#!join/" + serverBrowserBackend.spawned_server_links.invitation_code + "<br/>" +
                        "<b>IP address</b>: " + serverBrowserBackend.spawned_server_links.ip + "<br/><br/>" +
                        "Please note that spawned servers may not appear immediately. Wait a bit."
                messageDialog.icon = StandardIcon.Information
                messageDialog.open()
            }
        }

        onServerSpawnErrorChanged: {
            if (serverBrowserBackend.spawn_error) {
                messageDialog.title = "Error occured while server spawning"
                messageDialog.text = serverBrowserBackend.spawn_error
                messageDialog.icon = StandardIcon.Information
                messageDialog.open()
            }
        }
    }

    content: Item {
        anchors.fill: parent

        Timer {
            id: serversUpdateTimer

            running: visible ? true : false
            interval: 45000
            repeat: true
            triggeredOnStart: true

            onTriggered: serverBrowserBackend.update_servers()
        }

        Timer {
            id: myServersUpdateTimer

            running: visible ? true : false

            interval: 10000
            repeat: true
            triggeredOnStart: true

            onTriggered: serverBrowserBackend.update_my_servers()
        }

        QLLTabWidget {
            anchors.fill: parent


            Rectangle {
                property string title: "Servers"
                anchors.fill: parent
                color: "#e3e3e3"


                TableView {
                    id: tableView

                    anchors.fill: parent
                    model: rootItem.servers
                    clip:true
                    sortIndicatorVisible: true
                    sortIndicatorOrder: Qt.DescendingOrder
                    opacity: serverBrowserBackend.servers_update_state ? 0.5 : 1
                    enabled: serverBrowserBackend.servers_update_state ? false : true


//                    onSortIndicatorColumnChanged: serverBrowserBackend.sort_column(sortIndicatorColumn, sortIndicatorOrder)
//                    onSortIndicatorOrderChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)


                    onDoubleClicked: mainWindowBackend.open_server(rootItem.servers[currentRow]['link'])
                    onRowCountChanged: resizeColumnsToContents()

                    TableViewColumn {
                        role: "location";
                        title: "Location";

                        delegate: RowLayout {
                            spacing: 5
                            anchors.fill: parent

                            Image {
                                anchors.verticalCenter: parent.verticalCenter
                                source: rootItem.servers[styleData.row] ? rootItem.servers[styleData.row]['flag'] : ''
                                Layout.minimumHeight: 11
                                Layout.minimumWidth: 16

                                BusyIndicator {
                                    anchors.fill: parent
                                    height: parent.parent.height
                                    width: height
                                    visible: parent.status === Image.Loading
                                }
                            }
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                text: styleData.value
                                color: tableView.currentRow === styleData.row ? systemPalette.highlightedText : systemPalette.text
                                Layout.minimumWidth: implicitWidth + 10
                            }
                        }
                    }
                    TableViewColumn { role: "game_type" ; title: "Type"; delegate: defaultColumnDelegate }
                    TableViewColumn { role: "owner"  ; title: "Owner"; delegate: defaultColumnDelegate }
                    TableViewColumn { role: "name"  ; title: "Name"; delegate: defaultColumnDelegate }
                    TableViewColumn { role: "map"  ; title: "Map"; delegate: defaultColumnDelegate }
                    TableViewColumn { role: "players"  ; title: "Players"; delegate: defaultColumnDelegate }
                    TableViewColumn { role: "ip"  ; title: "Address"; delegate: defaultColumnDelegate }
                    TableViewColumn {
                        role: "protected"
                        title: "Password"

                        delegate: Row {
                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                text: styleData.value
                                color: tableView.currentRow === styleData.row ? systemPalette.highlightedText : systemPalette.text
                            }
                        }
                    }

                    rowDelegate: Rectangle {
                        height: 30
                        Behavior on height {
                            NumberAnimation { duration: 450
                                easing {
                                    type: Easing.OutElastic
                                    amplitude: 2.5
                                    period: 2.5
                                }
                            }
                        }

                        color: tableView.currentRow === styleData.row ? systemPalette.highlight : (styleData.alternate ? systemPalette.alternateBase : systemPalette.base)

                        MouseArea {
                            anchors.fill: parent
                            propagateComposedEvents: true
                            acceptedButtons: Qt.RightButton
                            onClicked: {
                                tableView.currentRow = styleData.row

                                if (tableView.currentRow >= 0) {
                                    serverContextMenu.popup()
                                }

                                mouse.accepted = false
                            }
                        }
                    }

                    Component { id: defaultColumnDelegate
                        RowLayout {
                            anchors.fill: parent

                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                text: styleData.value
                                color: tableView.currentRow === styleData.row ? systemPalette.highlightedText : systemPalette.text
                                Layout.minimumWidth: implicitWidth + 10
                            }
                        }
                    }

                    Menu {
                        id: serverContextMenu

                        MenuItem {
                            text: qsTr('View')

                            onTriggered: mainWindowBackend.open_server(rootItem.servers[tableView.currentRow]['link'])
                        }

                        MenuItem {
                            text: qsTr('Link')

                            onTriggered: {
                                messageDialog.title = "Invite link"
                                messageDialog.text = rootItem.servers[tableView.currentRow]['link']
                                messageDialog.icon = StandardIcon.Information
                                messageDialog.open()
                            }
                        }
                    }

                    MessageDialog {
                        id: messageDialog
                    }

                    BusyIndicator {
                        anchors.centerIn: parent
                        running: serverBrowserBackend.servers_update_state ? true : false
                        opacity: serverBrowserBackend.servers_update_state ? 1 : 0

                        Behavior on opacity {
                            NumberAnimation { duration: 1450
                                easing {
                                    type: Easing.OutElastic
                                    amplitude: 2.5
                                    period: 2.5
                                }
                            }
                        }
                    }
                }
            }

            Rectangle {
                property string title: "Filters"
                anchors.fill: parent
                color: "#e3e3e3"

                Column {
                    anchors.topMargin: 5
                    anchors.fill: parent
                    spacing: 5

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gamePlayers.values

                        currentText: filters.gamePlayers.selected

                        onCurrentTextChanged: filters.gamePlayers.selected = currentText
                    }

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gameType.values

                        currentText: filters.gameType.selected

                        onCurrentTextChanged: filters.gameType.selected = currentText
                    }

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gameState.values

                        currentText: filters.gameState.selected

                        onCurrentTextChanged: filters.gameState.selected = currentText
                    }

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gameDifficulty.values

                        currentText: filters.gameDifficulty.selected

                        onCurrentTextChanged: filters.gameDifficulty.selected = currentText
                    }

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gameLocation.values

                        currentText: ffilters.gameLocation.selected

                        onCurrentTextChanged: filters.gameLocation.selected = currentText
                    }

                    QLL.SmartComboBox {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        model: filters.gameMatches.values

                        currentText: filters.gameMatches.selected

                        onCurrentTextChanged: filters.gameMatches.selected = currentText
                    }

                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 5

                        Button {
                            id: resetFilterButton

                            text: "Reset"

                            onClicked: {
                                serverBrowserBackend.reset_filter()
                                filters = serverBrowserBackend.filters
                            }
                        }

                        Button {
                            id: applyFilterButton

                            text: "Apply"

                            onClicked: {
                                serverBrowserBackend.filters = rootItem.filters
                                serverBrowserBackend.update_servers()
                            }
                        }

                        Button {
                            id: saveFilterButton

                            text: "Save"

                            onClicked: {
                                serverBrowserBackend.filters = rootItem.filters
                                serverBrowserBackend.save_filter()
                            }
                        }
                    }
                }
            }

            Rectangle {
                property string title: "Spawn"
                anchors.fill: parent
                color: "#e3e3e3"

                Column {
                    anchors.topMargin: 5
                    anchors.fill: parent
                    spacing: 10
                    visible: serverBrowserBackend.subscription_level === "pro" ? true : false

                    Rectangle {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: serverBrowserBackend.my_servers ? 32 * spawnedServersListView.count : 0
                        opacity: serverBrowserBackend.my_servers ? 1 : 0
                        clip: true

                        color: "#333"

                        ListView { id: spawnedServersListView
                            anchors.fill: parent
                            model: serverBrowserBackend.my_servers
                            interactive: false

                            delegate: Item {
                                anchors.top: parent.top
                                anchors.left: parent.left
                                anchors.right: parent.right

                                height: 32

                                RowLayout {
                                    anchors.fill: parent
                                    anchors.margins: 5

                                    Marquee {
                                        anchors.verticalCenter: parent.verticalCenter
                                        Layout.fillWidth: true
                                        text: modelData.host_name + " | " + modelData.host_address + " | " + modelData.map + " | " + modelData.num_players + "/" + modelData.max_clients
                                        color: "lightgrey"
                                        font.pointSize: 12
                                    }

                                    ClickableText {
                                        anchors.verticalCenter: parent.verticalCenter
                                        Layout.minimumWidth: implicitWidth
                                        text: "Join"

                                        color: "lightgrey"
                                        defaultSize: 12
                                        pressedSize: 11

                                        onClicked: mainWindowBackend.open_server(modelData.public_id)
                                    }

                                    ClickableText {
                                        anchors.verticalCenter: parent.verticalCenter
                                        Layout.minimumWidth: implicitWidth
                                        text: "Stop"

                                        color: "lightgrey"
                                        defaultSize: 12
                                        pressedSize: 11

                                        onClicked: {
                                            serverBrowserBackend.stop_server(modelData.public_id)

                                            messageDialog.title = "Server stopped"
                                            messageDialog.text = "Server stopped successfully: " + modelData.host_name
                                            messageDialog.icon = StandardIcon.Information
                                            messageDialog.open()
                                        }
                                    }
                                }
                            }
                        }

                        Behavior on opacity {
                            NumberAnimation { duration: 600 }
                        }

                        Behavior on height {
                            NumberAnimation { duration: 450 }
                        }
                    }

                    Row {
                        height: spawnedServersListView.count > 0 ? 0 : implicitHeight
                        opacity: spawnedServersListView.count > 0  ? 0 : 1
                        anchors.horizontalCenter: parent.horizontalCenter
                        enabled: serverBrowserBackend.spawn_status === 0 ? true : false
                        spacing: 10
                        clip: true

                        QLL.SmartComboBox { id: locationComboBox
                            width: 130

                            model: spawnParameters["location"]["values"]

                            onCurrentTextChanged: {
                                spawnParameters["location"]["selected"] = currentText;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }

                        QLL.SmartComboBox { id: rulesetComboBox
                            width: 130

                            model: spawnParameters["ruleset"]["values"]

                            onCurrentTextChanged: {
                                spawnParameters["ruleset"]["selected"] = currentText;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }

                        QLL.SmartComboBox { id: gameTypeComboBox
                            width: 130

                            model: spawnParameters["game_type"]["values"]

                            onModelChanged: currentIndex = -1

                            Component.onCompleted: currentIndex = -1

                            onCurrentTextChanged: {
                                spawnParameters["game_type"]["selected"] = currentText;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }

                        QLL.SmartComboBox { id: mapComboBox
                            width: 130

                            onCurrentTextChanged: {
                                for (var key in spawnParameters["map"]["values"]) {
                                    if (spawnParameters["map"]["values"][key]["name"] === currentText) {
                                        spawnParameters["map"]["selected"] = spawnParameters["map"]["values"][key]["sysname"]
                                        break
                                    }
                                }
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }

                            onModelChanged: currentIndex = -1

                            Component.onCompleted: currentIndex = -1

                            Connections {
                                target: gameTypeComboBox
                                onCurrentIndexChanged: mapComboBox.updateMapsModelByGameType()
                            }

                            function updateMapsModelByGameType() {
                                if (gameTypeComboBox.currentIndex > -1) {
                                    var filteredMaps = []
                                    var selectedGameType = gameTypeComboBox.currentIndex

                                    for (var key in spawnParameters["map"]["values"]) {
                                        if (selectedGameType.toString() in spawnParameters["map"]["values"][key]["gametypes"]) {
                                            filteredMaps.push(spawnParameters["map"]["values"][key]["name"])
                                        }
                                    }

                                    mapComboBox.model = filteredMaps
                                }
                            }
                        }



                        Behavior on opacity {
                            NumberAnimation { duration: 600 }
                        }

                        Behavior on height {
                            NumberAnimation { duration: 450 }
                        }
                    }

                    Row {
                        height: spawnedServersListView.count > 0 ? 0 : implicitHeight
                        opacity: spawnedServersListView.count > 0  ? 0 : 1
                        anchors.horizontalCenter: parent.horizontalCenter
                        enabled: serverBrowserBackend.spawn_status === 0 ? true : false
                        spacing: 10
                        clip: true

                        TextField {
                            width: 150

                            placeholderText: "Name"

                            onTextChanged: {
                                spawnParameters["host_name"] = text;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }

                        TextField {
                            width: 150

                            placeholderText: "Password"

                            onTextChanged: {
                                spawnParameters["server_password"] = text;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }

                        TextField {
                            width: 150

                            placeholderText: "Invite your friends"

                            onTextChanged: {
                                spawnParameters["invites"] = text;
                                serverBrowserBackend.spawn_parameters = spawnParameters
                                serverBrowserBackend.update_packed_spawn_parameters()
                            }
                        }


                        Behavior on opacity {
                            NumberAnimation { duration: 600 }
                        }

                        Behavior on height {
                            NumberAnimation { duration: 450 }
                        }
                    }

                    TextArea {
                        height: spawnedServersListView.count > 0 ? 0 : 100
                        opacity: spawnedServersListView.count > 0  ? 0 : 1
                        enabled: serverBrowserBackend.spawn_status === 0 ? true : false
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: 400
                        clip: true
                        text: serverBrowserBackend.packed_spawn_parameters
                        textColor: serverBrowserBackend.spawn_parameters_has_error ? "red" : "green"

                        onTextChanged: serverBrowserBackend.parse_new_packed_parameters(text)


                        Behavior on opacity {
                            NumberAnimation { duration: 600 }
                        }

                        Behavior on height {
                            NumberAnimation { duration: 450 }
                        }
                    }

                    Button {
                        height: spawnedServersListView.count > 0 ? 0 : implicitHeight
                        opacity: spawnedServersListView.count > 0  ? 0 : 1
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: 150
                        clip: true

                        enabled: serverBrowserBackend.spawn_status === 0 ? true : false
                        text: serverBrowserBackend.spawn_status === 1 ? "Spawning..." : "Spawn"

                        onClicked: serverBrowserBackend.spawn_server()


                        Behavior on opacity {
                            NumberAnimation { duration: 600 }
                        }

                        Behavior on height {
                            NumberAnimation { duration: 450 }
                        }
                    }
                }

                Text {
                    visible: serverBrowserBackend.subscription_level === "pro" ? false : true
                    anchors.centerIn: parent
                    text: "You  don't have a <b>pro</b> account"
                    font.pointSize: 14
                }
            }
        }
    }

    SystemPalette {
        id: systemPalette
    }
}
