/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.2
import QtQuick.Controls 1.1

import "../uihelpers.js" as UIHelpers


Item {
    id: rootItem

    width: 100
    height: 24

    property alias editable: comboBox.editable
    property alias model: comboBox.model
    property alias currentIndex: comboBox.currentIndex
    property string currentText

    function setValidItemByCurrentText() {
        var index = comboBox.find(rootItem.currentText)

        if (index > -1) {
            comboBox.currentIndex = index;
        }
    }

    function updateCurrentTextIfNeed() {
        if (rootItem.currentText.length && rootItem.currentText != comboBox.currentText) {
            setValidItemByCurrentText()
        } else {
            rootItem.currentText = comboBox.currentText
        }
    }

    onCurrentTextChanged: rootItem.updateCurrentTextIfNeed()

    ComboBox {
        id: comboBox

        anchors.fill: parent
        activeFocusOnPress: true
        z: 1

        onCountChanged: rootItem.updateCurrentTextIfNeed()

        onCurrentTextChanged: {
            // This should prevent excess "0" in current text
            if (model[currentIndex] === currentText) {
                rootItem.updateCurrentTextIfNeed()
            }
        }

        onActivated: rootItem.currentText = comboBox.model[index]
    }
}
