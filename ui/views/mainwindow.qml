/*
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtGraphicalEffects 1.0
import QtMultimedia 5.0

import fx.qml.helpers 1.0
import fx.qml.controls 1.0

import "controls" as QL
import "uihelpers.js" as UIHelpers
//import "PyComponents" 0.1 as QLC
// Components below will be registered from python
import MainWindowBackend 1.0
import UpdateCheckerBackend 1.0
import ServerBrowserBackend 1.0
import FriendsWidgetBackend 1.0
import ProfileWidgetBackend 1.0



ApplicationWindow {
    id: root

    /* propeties */
    visible: true
    width: 720
    height: 405
    maximumWidth: width
    maximumHeight: height
    minimumWidth: width
    minimumHeight: height
    flags: Qt.FramelessWindowHint
    title: qsTr("QuakeLive Launcher")


    x: (Screen.width - width) / 2
    y: (Screen.height - height) / 2


    /* Signals */
    signal playButtonClicked()
    signal serverOpenRequested(string serverString)
    signal browserOpenRequested()
    signal friendsOpenRequested()
    signal windowClosing()


    onWindowClosing: {
        audioSystem.muted = true
    }

    function exitLauncher() {
        root.windowClosing()
        root.close()
    }

    MouseArea {
        id: mouseArea

        anchors.fill: parent
        enabled: stackView.depth > 1


        onClicked: {
//            stackView.popAllItems()
            stackView.popItem()
        }
    }

    /* Childs */
    StackView { id: stackView

        initialItem: rootContentItem

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.visible = true
                if (stackView.depth > 2) {
                    properties.exitItem.scale = 0.8
                } else {
                    properties.exitItem.scale = 1
                }
            }

            pushTransition: StackViewTransition {
                ParallelAnimation {
                    PropertyAnimation {
                        target: enterItem
                        property: "opacity"
                        from: 0
                        to: 1
                        duration: 250
                    }

                    PropertyAnimation {
                        target: enterItem
                        property: "x"
                        from: root.width
                        to: root.width - enterItem.maxWidth
                        duration: 450
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }

                    PropertyAnimation {
                        target: enterItem
                        property: "width"
                        from: 0
                        to: enterItem.maxWidth
                        duration: 450
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }

                    PropertyAnimation {
                        target: exitItem
                        property: "scale"
                        from: 1
                        to: stackView.depth > 2 ? 0.8 : 1
                        duration: 650
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }
                }
            }

            popTransition: StackViewTransition {
                ParallelAnimation {
                    PropertyAnimation {
                        target: enterItem
                        property: "scale"
                        from: stackView.depth > 1 ? 0.8 : 1
                        to: 1
                        duration: 450
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }

                    PropertyAnimation {
                        target: exitItem
                        property: "opacity"
                        from: 1
                        to: 0
                        duration: 250
                    }

                    PropertyAnimation {
                        target: exitItem
                        property: "x"
                        from: root.width - exitItem.maxWidth
                        to: root.width
                        duration: 450
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }

                    PropertyAnimation {
                        target: exitItem
                        property: "width"
                        to: 0
                        duration: 450
                        easing {
                            type: Easing.OutElastic
                            amplitude: 2.5
                            period: 2.5
                        }
                    }
                }
            }
        }

        function pushItem(item) {
            stackView.currentItem.enabled = false
            stackView.push(item)
            stackView.currentItem.forceActiveFocus()
        }

        function popItem() {
            stackView.pop()
            stackView.currentItem.enabled = true
            stackView.currentItem.forceActiveFocus()
        }

        function popAllItems() {
            stackView.pop(null)
            stackView.currentItem.forceActiveFocus()
        }
    }

    Action {
        id: openAction
        shortcut: StandardKey.Paste
        onTriggered: {
            mainWindow.open_server(clipboard.text)
        }
    }

    FocusScope {
        id: rootContentItem

        focus: true

        Keys.onEscapePressed: {
            if (exitWidget !== stackView.currentItem) {
                stackView.pushItem(exitWidget)
            }
            event.accepted = true;
        }

        property var quickActionFire


        Action {
            shortcut: "Ctrl+L"
            text: "QuakeLive Launcher Commands"
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.parameterDelimiter = ""
                quickAction.oneWord = true
                quickAction.noSuggestionFoundText = "No such command"
                quickAction.suggestions = {
                    "practice": "Launch game in practice mode",
                    "quit": "Exit the launcher",
                    "exit": "Exit the launcher",
                }

                if (!mainWindow.is_connected) {
                    quickAction.suggestions["connect"] = "Connect to QuakeLive Network"
                }

                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()

                    if (actualText.trim() === "practice") {
                        practiceButton.clicked()
                    } else if (UIHelpers.stringStartsWith(actualText, "connect")) {
                        connectImage.clicked()
                    } else if (actualText.trim() === "quit" || actualText.trim() === "exit") {
                        exitLauncher()
                    }
                }

                quickAction.forceActiveFocus()
            }
        }

        Action {
            shortcut: "Ctrl+S"
            text: "Spawn a server"
            enabled: mainWindow.is_connected
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.oneWord = false
                quickAction.parameterDelimiter = ""
                quickAction.suggestions = serverBrowser.quick_spawn_server_list
                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()

//                    serverBrowserWidget.quick_spawn(actualText)
                }

                quickAction.forceActiveFocus()
            }
        }

        Action {
            shortcut: "Ctrl+J"
            text: "Join a server"
            enabled: mainWindow.is_connected
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.parameterDelimiter = ""
                quickAction.oneWord = false
//                quickAction.suggestions = quickActionsBackend.join_server_choices
                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()
                }

                quickAction.forceActiveFocus()
            }
        }

        Action {
            shortcut: "Ctrl+I"
            text: "Invite a friend to " +  UIHelpers.getItemIfExists(friends.owner_currently_on_server, "ADDRESS", "")
            enabled: friends.owner_currently_on_server ? true : false
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.parameterDelimiter = ""
                quickAction.oneWord = true
                quickAction.suggestions = friends.quick_invite_list
                quickAction.noSuggestionFoundText = "No such friend online"
                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()
                    friends.invite_user_to_current_server(actualText)

                    messageDialog.title = "Invited"
                    messageDialog.text = "User has been invited to your server: <b>" + actualText + "</b>"
                    messageDialog.icon = StandardIcon.Information
                    messageDialog.open()
                }

                quickAction.forceActiveFocus()
            }
        }

        Action {
            shortcut: "Ctrl+G"
            text: "Change your game settings" //cvars only supported
            enabled: mainWindow.is_connected
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.parameterDelimiter = " "
                quickAction.oneWord = false
                quickAction.noSuggestionFoundText = "No such config variable"
                quickAction.suggestions = mainWindow.game_config
                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()

                    if (quickAction.args.length > 1) {
                        var command = quickAction.args[0]
                        var value = quickAction.args[1]

                        if (value) value = value.trim()

                        console.log('Set "' + command + '" to: ' + value)
                    }
                }

                quickAction.forceActiveFocus()
            }
        }

        Action {
            shortcut: "Ctrl+P"
            text: "View profile of a player"
            onTriggered: {
                quickAction.clear()
                quickAction.blankValueText = text
                quickAction.parameterDelimiter = ""
                quickAction.oneWord = true
                quickAction.suggestions = serverBrowser.quick_spawn_server_list
                quickAction.noSuggestionFoundText = "View profile"
                rootContentItem.quickActionFire = function(actualText, suggestedText) {
                    quickAction.close()
                    friendsWidget.profileOpenRequested(actualText)
                }

                quickAction.forceActiveFocus()
            }
        }

        Item {
            id: blurableContentItem

            anchors.fill: parent
            enabled: rootContentItem.activeFocus

            layer.enabled: true
            layer.effect: FastBlur {
                radius: rootContentItem.activeFocus ? 0 : 32

                Behavior on radius {
                    NumberAnimation { duration: 750 }
                }
            }

            Image {
                id: background
                anchors.fill: parent
                source: "../images/" + UIHelpers.getRandomInt(1, 3) + ".jpg"
            }

            Text {
                id: aboutText
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                color: "#ccc"
                text: mainWindow.about_text
                Layout.fillHeight: true
                verticalAlignment: Text.AlignTop
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                font.pointSize: 14
            }

            QL.QLLMainMenu {
                id: mainMenuWidget

                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: mainWindow.is_connected ? 400 : 0
                height: mainWindow.is_connected ? mainMenuWidget.maxMenuHeight : 0
                opacity: mainWindow.is_connected ? 1 : 0
                clip: true
                showFriendsNotification: friends.has_notifications

                myServers: mainWindow.my_servers

                Behavior on height {
                    NumberAnimation { duration: 250
                        easing {
                            type: Easing.OutElastic
                            amplitude: 0.5
                            period: 2.5
                        }
                    }
                }

                Behavior on opacity {
                    NumberAnimation { duration: 550
                        easing {
                            type: Easing.OutElastic
                            amplitude: 0.5
                            period: 2.5
                        }
                    }
                }

                Behavior on width {
                    NumberAnimation { duration: 250
                        easing {
                            type: Easing.OutElastic
                            amplitude: 0.5
                            period: 2.5
                        }
                    }
                }
            }

            QL.ClickableImage {
                id: settingsButton

                width: 48
                height: 48
                anchors.left: parent.left
                anchors.bottom: parent.bottom

                source: "../images/settings.png"

                ColorOverlay {
                    anchors.fill: settingsButton
                    source: settingsButton
                    color: "#ddd"
                }

                onClicked: stackView.pushItem(settingsWidget)
            }

            QL.ClickableImage {
                id: connectImage

                width: 48
                height: 48
                anchors.left: parent.left
                anchors.top: parent.top

                rotateOnHover: false
                source: "../images/network.png"
                isBouncing: mainWindow.is_connecting
                helpTextOnHover: false
                helpText: mainWindow.status_text
                helpTextColor: "#ddd"
                helpTextFont.pointSize: 10


                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: mainWindow.is_connected ? "#0d0" : "#d00"
                }

                onClicked: {
                    if (!mainWindow.is_connected && !mainWindow.is_connecting) {
                        mainWindow.reconnect()
                    }
                }
            }

            QL.ClickableImage {
                id: currentServerImage

                width: friends.owner_currently_on_server ? 48 : 0
                height: friends.owner_currently_on_server ? 48 : 0
                anchors.left: connectImage.right
                anchors.top: parent.top

                rotateOnHover: false
                source: "../images/play.png"
                helpText: "Current server"
                helpTextColor: "#ddd"
                helpTextFont.pointSize: 10
                clip: true

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#ddd"
                }

                Behavior on height {
                    NumberAnimation { duration: 500
                        easing {
                            type: Easing.OutElastic
                            amplitude: 0.5
                            period: 2.5
                        }
                    }
                }

                Behavior on width {
                    NumberAnimation { duration: 500
                        easing {
                            type: Easing.OutElastic
                            amplitude: 0.5
                            period: 2.5
                        }
                    }
                }

                onClicked: if (friends.owner_currently_on_server) { clipboard.text = "http://quakelive.com/#!join/" + friends.owner_currently_on_server['SERVER_ID'] }
            }

            QL.ClickableImage {
                id: practiceButton

                width: 48
                height: 48
                anchors.left: parent.left
                anchors.top: connectImage.bottom

                rotateOnHover: false
                bounceOnClicked: true
                bounceOnClickedInterval: 3500
                source: "../images/practice.png"
                helpTextOnHover: false
                helpText: "Practice"
                helpTextColor: "#ddd"
                helpTextFont.pointSize: 10


                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#ddd"
                }

                onClicked: mainWindow.launch_practice()
            }

            QL.ClickableImage {
                id: exitButton

                width: 32
                height: 32
                anchors.right: parent.right
                anchors.top: parent.top

                source: "../images/close.png"

                rotateOnHover: false

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#ddd"
                }

                onClicked: stackView.pushItem(exitWidget)
            }

            QL.ClickableImage {
                id: donateButton

                width: 32
                height: 32
                anchors.left: settingsButton.right
                anchors.verticalCenter: settingsButton.verticalCenter

                rotateOnHover: false
                helpText: "Donate"
                helpTextColor: "#ddd"
                helpTextFont.pointSize: 11
                helpTextShowOnChange: false
                source: "../images/donate.png"

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#ddd"
                }

                onClicked: Qt.openUrlExternally("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=DVHN3DL3WRTSC&lc=RU&item_name=Victor%20Polevoy%20%28QLLauncher%29&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted")
            }

            QL.ClickableImage {
                id: streamsButton

                width: 48
                height: 48
                anchors.bottom: settingsButton.top
                anchors.left: parent.left

                rotateOnHover: false
                helpText: "Streams"
                helpTextColor: "#ddd"
                helpTextFont.pointSize: 11
                helpTextShowOnChange: false
                source: "../images/tv.png"

                ColorOverlay {
                    anchors.fill: streamsButton
                    source: streamsButton
                    color: "#ddd"
                }

                onClicked: {
                    stackView.pushItem(streamsWidget)
                }
            }
        }

        Item {
            id: nonBlurableContentItem

            anchors.fill: parent


            QL.QLLSimpleDataWidget {
                id: simpleDataWidget

                z: 1

                clip: true

                onWidgetClosing: stackView.popItem()
            }

            QL.QLLSettingsWidget {
                id: settingsWidget
                clip: true
                z: 4

                property bool updateAfterClose: false
                settingsDictionary: mainWindow.settings_dictionary
                onWidgetClosing: {
                    stackView.popItem()
                    mainWindow.settings_dictionary = settingsWidget.settingsDictionary

                    if (updateAfterClose) {
                        updateCheckerWidget.visible = true
                        updateAfterClose = false
                        updateChecker.check_update()
                    }
                }

                Stack.onStatusChanged: if (Stack.status === Stack.Deactivating) { widgetClosing() }

                Keys.onEscapePressed: {
                    widgetClosing()
                    event.accepted = true
                }
            }

            QL.QLLServerBrowserWidget {
                id: serverBrowserWidget

                clip: true
                serverBrowserBackend: serverBrowser
                mainWindowBackend: mainWindow

                Keys.onEscapePressed: {
                    stackView.popItem()
                    event.accepted = true
                }

                onWidgetClosing: stackView.popItem()
            }

            QL.QLLFriendsWidget {
                id: friendsWidget

                friendsWidgetBackend: friends

                clip: true

                Keys.onEscapePressed: {
                    stackView.popItem()
                    event.accepted = true
                }

                onWidgetClosing: stackView.popItem()
            }

            QL.QLLExitWidget {
                id: exitWidget

                z: 10

                clip: true

                onExitRequested: exitLauncher()

                onWidgetClosing: stackView.popItem()
            }

            QL.QLStreamsWidget {
                id: streamsWidget

                model: mainWindow.live_streams
                mainWindowBackend: mainWindow
                clip: true

                Keys.onEscapePressed: {
                    stackView.popItem()
                    event.accepted = true
                }

                onWidgetClosing: stackView.popItem()
            }

            QL.QLProfileWidget {
                id: profileWidget

                profileWidgetBackend: profile
                clip: true

                Keys.onEscapePressed: {
                    widgetClosing()
                    event.accepted = true
                }

                onWidgetClosing: stackView.popItem()
            }
        }


        Connections {
            target: mainMenuWidget

            onServerOpenRequested: mainWindow.open_server(serverString)
            onBrowserRequested: stackView.pushItem(serverBrowserWidget)
            onFriendsRequested: stackView.pushItem(friendsWidget)
            onProfileRequested: {
                stackView.pushItem(profileWidget)
                profile.target_profile = mainWindow.my_nick
                profile.profile_request_type = "summary"
            }
        }

        Connections {
            target: friendsWidget

            onOpenServerRequested: mainWindow.open_server(serverId)
            onProfileOpenRequested: {
                profile.target_profile = name
                profile.profile_request_type = "summary"

                if (stackView.currentItem !== profileWidget) {
                    stackView.pushItem(profileWidget)
                }
            }
        }

        Connections {
            target: simpleDataWidget

            onJoinButtonClicked: mainWindow.join_current_server()
        }

        Connections {
            target: mainWindow

            onCurrentServerChanged: {
                if (mainWindow.current_server) {
                    simpleDataWidget.widgetData = mainWindow.current_server

                    stackView.pushItem(simpleDataWidget)
                }
            }

            onIsConnectingChanged: {
                if (!mainWindow.is_connecting && !mainWindow.is_connected) {
                    audioSystem.playSound("../../sounds/failure.mp3")
                }
            }

            onIsConnectedChanged: {
                if (mainWindow.is_connected) {
                    audioSystem.playSound("../../sounds/success.mp3")
                }
            }
        }
    }

    QL.QLLUpdateChecker {
        id: updateCheckerWidget

        z: 3

        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        clip: true

        updateCheckerBackend: updateChecker

        onSpecifyExisting: { visible = false; stackView.pushItem(settingsWidget); settingsWidget.updateAfterClose = true }

        onWidgetClosing: stackView.popItem()
    }

    QuickAction { id: quickAction
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2 - 100

        opacity: quickAction.textInputItem.activeFocus ? 0.7 : 0
        height: quickAction.textInputItem.activeFocus ? 42 : 0
        clip: height ? false : true
        width: 620

        textInputItem.font.pixelSize: 21
        showSuggestionList: true


        Behavior on height {
            NumberAnimation { duration: 500
                easing {
                    type: Easing.OutElastic
                    amplitude: 2.5
                    period: 2.5
                }
            }
        }

        Behavior on opacity {
            NumberAnimation { duration: 1500
                easing {
                    type: Easing.OutElastic
                    amplitude: 2.5
                    period: 2.5
                }
            }
        }

        onActionRequest: {
            rootContentItem.quickActionFire(actualText, suggestedText);
            stackView.currentItem.forceActiveFocus()
        }

        onClose: stackView.currentItem.forceActiveFocus()

        MessageDialog {
            id: messageDialog
        }
    }


    Connections {
        target: root

        onWindowClosing: mainWindow.close_window()
    }

    Connections {
        target: clipboard

        onDataChanged: mainWindow.open_server(clipboard.text)
    }

    Connections {
        target: friends

        onMessageReceived: audioSystem.playSound("../../sounds/notification.mp3")
        onMessageSent: audioSystem.playSound("../../sounds/message_sent.mp3")
    }

    Audio {
        id: audioSystem

        loops: 1
        muted: mainWindow.settings_dictionary.sound_muted === "True"
        volume: mainWindow.settings_dictionary.sound_volume / 100

        function playSound(soundSource) {
            this.stop()
            this.source = soundSource
            this.play()
        }
    }


    MainWindowBackend {
        id: mainWindow
    }

    UpdateCheckerBackend {
        id: updateChecker
    }

    ServerBrowserBackend {
        id: serverBrowser
    }

    FriendsWidgetBackend {
        id: friends
    }

    ProfileWidgetBackend {
        id: profile
    }

    Clipboard {
        id: clipboard
    }
}
