"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


import logging
from PyQt5 import QtWidgets, QtWidgets, QtCore
from PyQt5.QtCore import pyqtProperty, QCoreApplication, QObject, QUrl
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine


class CommonQMLWindow():

    def __init__(self, qml_file_name=None):
        if qml_file_name:
            self._engine = QQmlEngine()
            component = QQmlComponent(self._engine)
            component.loadUrl(QUrl(qml_file_name))
            self.qml_window = component.create()

            if self.qml_window is None:
                for error in component.errors():
                    logging.error(error.toString())

                exit(1)