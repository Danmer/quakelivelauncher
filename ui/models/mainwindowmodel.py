"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'

from helpers import settings
from helpers import qtthreaddecorator
import qllauncher as qll

import requests
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, pyqtProperty, QVariant


class MainWindowModel(QObject):
    _my_servers_pattern = '%s -=- %s -=- %s (%s) -=- %s'
    _my_servers_regexp = '(.*) -=- ((\d{1,3}).(\d{1,3}).(\d{1,3}).(\d{1,3}):(\d{3,5})) -=- (.*)'

    isConnectedChanged = pyqtSignal()
    liveStreamsChanged = pyqtSignal()
    isConnectingChanged = pyqtSignal()
    myServersChanged = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)

        self._ql_network = None
        self._my_servers = None
        self._live_streams = None
        self._is_connected = False
        self._is_connecting = False

    # Properties
    # is_connected property
    @pyqtProperty('bool', notify=isConnectedChanged)
    def is_connected(self):
        return self._is_connected

    @is_connected.setter
    def is_connected(self, is_connected):
        self._is_connected = is_connected
        self.isConnectedChanged.emit()

    # is_connecting property
    @pyqtProperty('bool', notify=isConnectingChanged)
    def is_connecting(self):
        return self._is_connecting

    @is_connecting.setter
    def is_connecting(self, is_connecting):
        self._is_connecting = is_connecting
        self.isConnectingChanged.emit()

    # my_servers property
    @pyqtProperty('QVariant', notify=myServersChanged)
    def my_servers(self):
        return QVariant(self._my_servers)

    @my_servers.setter
    def my_servers(self, my_servers):
        self._my_servers = my_servers
        self.myServersChanged.emit()

    # live_streams property
    @pyqtProperty('QVariant', notify=liveStreamsChanged)
    def live_streams(self):
        return QVariant(self._live_streams)

    @live_streams.setter
    def live_streams(self, live_streams):
        self._live_streams = live_streams
        self.liveStreamsChanged.emit()


    # Private
    def _get_ql_handle(self, gt_user, gt_pass, ql_session):
        settings_dict = settings.Settings().config_dict
        return qll.QLHandle(settings_dict['quake_live_binary_path'],
                            settings_dict['quake_live_home_path'],
                            settings_dict['quake_live_base_path'],
                            gt_user,
                            gt_pass,
                            ql_session,
                            settings_dict['taskset'] == 'True',
                            settings_dict['wineskin_path'])

    def _get_ql_handle_practice(self):
        return self._get_ql_handle('UnnamedPlayer', None, None)

    def _connect_to_ql_network(self):
        @qtthreaddecorator.qt_thread_decorator()
        def _connect_in_thread():
            self.is_connecting = True
            settings_dict = settings.Settings().config_dict
            self._ql_network = qll.QLNetwork()
            self._ql_network.connect(email=settings_dict['email'], password=settings_dict['password'])
            self.my_servers = self._ql_network.get_spawned_servers_list()

        def _check_connected(error=None):
            self.is_connecting = False

            if error:
                self.disconnect()
            else:
                self.is_connected = True

        _connect_in_thread().finished_signal.connect(_check_connected)

    # Public
    def connect(self):
        if not self._is_connected:
            self._connect_to_ql_network()

    def disconnect(self):
        if self._ql_network:
            self._ql_network.disconnect()
            self._ql_network = None

        if self._is_connected:
            self.is_connected = False

    def reconnect(self):
        self.disconnect()
        self.connect()

    def play(self, server):
        ql_handle = self._get_ql_handle(self._ql_network.gt_user,
                                        self._ql_network.gt_pass,
                                        self._ql_network.ql_session)

        if 'connect_string' in server.keys():
            ql_handle.launch(server['connect_string'])
        else:
            connect_string = self._ql_network.get_connect_string(server['host_address'])

            if not connect_string:
                raise AttributeError('This link is no longer valid')
            else:
                ql_handle.launch(connect_string)    # can throw EnvironmentError

    def launch_practice(self):
        ql_handle = self._get_ql_handle_practice()

        ql_handle.launch()

    def get_server_data(self, server_string):
        return self._ql_network.get_server_info(server_string)

    def get_connect_string(self, server_string):
        return self._ql_network.get_connect_string(server_string)

    def get_streams(self, tag, limit):
        api_url = 'https://api.twitch.tv/kraken/search/streams?limit=%d&q=%s' % (limit, tag)
        twitch_live_streams = requests.get(api_url).json()

        live_streams = []

        try:
            for live_stream in twitch_live_streams['streams']:
                stream_name = live_stream['channel']['display_name']
                stream_status = 'online'
                stream_url = live_stream['channel']['url']
                stream_viewers = live_stream['viewers']
                stream_preview_image = live_stream['preview']['medium']

                live_streams.append({
                    'name': stream_name,
                    'status': stream_status,
                    'url': stream_url,
                    'viewers': stream_viewers,
                    'preview_image': stream_preview_image
                })
        except KeyError:
            print("Can't retrieve any live streams from twitch.tv: api has been changed.")

        return live_streams

    def get_game_config(self):
        if self._is_connected:
            return qll.QLNetwork().ql_user_info['CVARS']

        return None