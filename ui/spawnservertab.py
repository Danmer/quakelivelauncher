#TODO DELETE THIS FILE
'''
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

__author__ = 'Victor Polevoy'

from helpers import qlnetwork, settings
from helpers.helpers import misc, qtthreaddecorator
from ui import progressdialog

from PyQt5 import QtWidgets, QtCore, QtGui


class SpawnServerTab(QtCore.QObject):
    _my_servers_pattern = '%s -=- %s -=- %s (%s) -=- %s'
    _my_servers_regexp = '(.*) -=- ((\d{1,3}).(\d{1,3}).(\d{1,3}).(\d{1,3}):(\d{3,5})) -=- (.*)'

    def __init__(self, parent):
        QtCore.QObject.__init__(self, parent)
        self._current_spawned_server = None
        self._spawned_servers_list = []

        self._location_index = 0
        self._game_type_index = 0
        self._rule_set_index = 0

        # filling widgets with data
        self._fill_locations()
        self._fill_rule_sets()
        self._fill_game_types()
        self._fill_spawned_servers_list()

        # changed handlers
        self.parent().location_combo_box.currentIndexChanged.connect(self._text_changed)
        self.parent().ruleset_combo_box.currentIndexChanged.connect(self._text_changed)
        self.parent().game_type_combo_box.currentIndexChanged.connect(self._text_changed)
        self.parent().map_edit.textChanged.connect(self._text_changed)
        self.parent().host_name_edit.textChanged.connect(self._text_changed)
        self.parent().password_edit.textChanged.connect(self._text_changed)
        self.parent().invites_edit.textChanged.connect(self._text_changed)

        # clicked handlers
        self.parent().spawn_button.clicked.connect(self._spawn_button_clicked)
        self.parent().stop_server_button.clicked.connect(self._stop_server_button_clicked)
        self.parent().get_links_button.clicked.connect(self._get_links_button_clicked)
        self.parent().refresh_spawned_servers_button.clicked.connect(self._fill_spawned_servers_list)
        self.parent().spawned_servers_list_view.clicked.connect(self._on_spawned_server_changed)

        self._text_changed()
        self._on_spawned_server_changed(None)

    def _on_spawned_server_changed(self, index):
        if index:
            self._current_spawned_server = self._spawned_servers_list[index.row()]
            self.parent().get_links_button.setEnabled(True)
            self.parent().stop_server_button.setEnabled(True)
        else:
            self._current_spawned_server = None
            self.parent().get_links_button.setEnabled(False)
            self.parent().stop_server_button.setEnabled(False)

    def _get_links_button_clicked(self):
        if self._current_spawned_server:
            QtWidgets.QMessageBox.information(self.parent(),
                                              'Server links', '%s%s' % (qlnetwork.QLNetwork._quake_live_join_link_pattern,
                                                                    self._current_spawned_server['public_id']))

    def _fill_spawned_servers_list(self):
        if self.parent().spawned_servers_list_view.model():
            self.parent().spawned_servers_list_view.model().clear()
            self._on_spawned_server_changed(None)

        try:
            self._spawned_servers_list = qlnetwork.QLNetwork().get_spawned_servers_list(True)

            model = QtGui.QStandardItemModel()

            for server in self._spawned_servers_list:
                players = qlnetwork.QLNetwork.parse_players_count(server)

                server_item = QtGui.QStandardItem(SpawnServerTab._my_servers_pattern % (
                    server['host_name'],
                    server['host_address'],
                    qlnetwork.QLNetwork.game_types[server['game_type']],
                    '%s/%s' % (players[0], players[2]),
                    server['map']
                ))

                model.appendRow(server_item)

            self.parent().spawned_servers_list_view.setModel(model)
        except KeyError:
            pass

    def _stop_server_button_clicked(self):
        if self._current_spawned_server:
            warning_message = "Are you sure you want to stop the server: %s?" % \
                              self._current_spawned_server['host_address']

            reply = QtWidgets.QMessageBox.question(self.parent(),
                                               'Confirm action',
                                               warning_message,
                                               QtWidgets.QMessageBox.Yes,
                                               QtWidgets.QMessageBox.No)

            if reply == QtWidgets.QMessageBox.Yes:
                dialog_label_text = 'Stopping server: %s' % self._current_spawned_server['host_address']
                stop_server_progress_dialog = progressdialog.ProgressDialog(self.parent(), dialog_label_text)
                stop_server_progress_dialog.show()

                @qtthreaddecorator.qt_thread_decorator()
                def _stop_server():
                    qlnetwork.QLNetwork().send_stop_server_and_wait_until_it_really_stop(self._current_spawned_server['public_id'])

                def _on_server_stopped():
                    self._fill_spawned_servers_list()
                    stop_server_progress_dialog.cancel()


                _stop_server().finished.connect(_on_server_stopped)

    def _fill_locations(self):
        sorted_locations = sorted(qlnetwork.QLNetwork.locations.values())
        for item in sorted_locations:
            if item != qlnetwork.QLNetwork.locations['any'] and item != qlnetwork.QLNetwork.locations['ALL']:
                self.parent().location_combo_box.addItem(item)

    def _fill_rule_sets(self):
        for item in qlnetwork.QLNetwork.rule_sets.values():
            self.parent().ruleset_combo_box.addItem(item)

    def _fill_game_types(self):
        for item in qlnetwork.QLNetwork.game_types.values():
            if item != qlnetwork.QLNetwork.game_types['any']:
                self.parent().game_type_combo_box.addItem(item)

    def _text_changed(self):
        location = misc.dict_get_key_by_value(qlnetwork.QLNetwork.locations,
                                              self.parent().location_combo_box.currentText())
        game_type = misc.dict_get_key_by_value(qlnetwork.QLNetwork.game_types,
                                               self.parent().game_type_combo_box.currentText())
        rule_set = misc.dict_get_key_by_value(qlnetwork.QLNetwork.rule_sets,
                                              self.parent().ruleset_combo_box.currentText())
        maps = self.parent().map_edit.text().split(',')
        host_name = self.parent().host_name_edit.text()
        server_password = self.parent().password_edit.text()
        invites = self.parent().invites_edit.text().split(',')

        packed_settings = qlnetwork.QLNetwork.get_packed_server_settings(location,
                                                                         game_type,
                                                                         maps,
                                                                         server_password,
                                                                         host_name,
                                                                         rule_set,
                                                                         invites)

        self.parent().imported_settings_edit.setText(str(packed_settings))

    def _spawn_button_clicked(self):
        imported_settings = eval(self.parent().imported_settings_edit.toPlainText())

        if imported_settings:
            servers_count = int(self.parent().server_count_combo_box.currentText())
            location_name = qlnetwork.QLNetwork.locations[imported_settings['cvars']['web_location']]
            dialog_label_text = 'Spawning %d server(s) at %s' % (servers_count, location_name)
            spawn_progress_dialog = progressdialog.ProgressDialog(self.parent(), dialog_label_text)
            spawn_progress_dialog.show()

            def _on_spawn_finish():
                spawn_progress_dialog.cancel()

            try:
                @qtthreaddecorator.qt_thread_decorator()
                def _spawn_server():
                    qlnetwork.QLNetwork().spawn_server_with_settings(imported_settings, True)

                def _on_server_spawned(status):
                    _on_spawn_finish()

                    if status:
                        QtWidgets.QMessageBox.critical(None, 'Error while spawning', status)
                    else:
                        self._fill_spawned_servers_list()

                for server_number in range(0, servers_count):
                    _spawn_server().finished_signal.connect(_on_server_spawned)
            except KeyError as e:
                _on_spawn_finish()
                QtWidgets.QMessageBox.critical(None, 'Error while spawning', str(e))