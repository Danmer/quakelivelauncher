"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


from helpers import qtthreaddecorator, settings, __version__ as qllauncher_version
from qllauncher import qlnetwork as qln
from ui.models import mainwindowmodel
from PyQt5.QtCore import pyqtProperty, QObject, pyqtSlot, pyqtSignal, QVariant
from PyQt5 import QtWidgets


class MainWindowBackend(QObject):
    statusChanged = pyqtSignal()
    myServersChanged = pyqtSignal()
    liveStreamsChanged = pyqtSignal()
    settingsDictionaryChanged = pyqtSignal()
    isBusyChanged = pyqtSignal()
    isConnectedChanged = pyqtSignal()
    isConnectingChanged = pyqtSignal()
    statusTextChanged = pyqtSignal()
    currentServerChanged = pyqtSignal()
    streamsIsUpdatingChanged = pyqtSignal()

    def __init__(self, parent=None):
        QObject.__init__(self)

        self._model = mainwindowmodel.MainWindowModel()
        self._model.isConnectedChanged.connect(self.isConnectedChanged)
        self._model.isConnectingChanged.connect(self.isConnectingChanged)
        self._model.myServersChanged.connect(self.myServersChanged)
        self._model.liveStreamsChanged.connect(self.liveStreamsChanged)

        self._about_text = 'QLLauncher <strong>%s</strong> by <strong>FX</strong>' % qllauncher_version

        self._status = False
        self._status_text = 'Press to connect'
        self._current_server = None
        self._streams_is_updating = False
        self.isConnectedChanged.connect(self._on_is_connected_changed)
        self.isConnectingChanged.connect(self._on_is_connecting_changed)

    # about_text property
    @pyqtProperty('QString', constant=True)
    def about_text(self):
        return self._about_text

    # my_nick property
    @pyqtProperty(str, constant=True)
    def my_nick(self):
        if qln.QLNetwork().is_connected():
            return qln.QLNetwork().gt_user

        return None

    # status property
    @pyqtProperty('bool', notify=statusChanged)
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status
        self.statusChanged.emit()

    # streams_update_status property
    @pyqtProperty('bool', notify=streamsIsUpdatingChanged)
    def streams_is_updating(self):
        return self._streams_is_updating

    @streams_is_updating.setter
    def streams_is_updating(self, streams_is_updating):
        self._streams_is_updating = streams_is_updating
        self.streamsIsUpdatingChanged.emit()

    # settings_dictionary property
    @pyqtProperty('QVariant', notify=settingsDictionaryChanged)
    def settings_dictionary(self):
        return QVariant(settings.Settings().config_dict)

    @settings_dictionary.setter
    def settings_dictionary(self, settings_dictionary):
        settings.Settings().write_config(settings_dictionary)
        self.settingsDictionaryChanged.emit()

    # live_streams property
    @pyqtProperty('QVariant', notify=liveStreamsChanged)
    def live_streams(self):
        return self._model.live_streams

    # my_servers property
    @pyqtProperty('QVariant', notify=myServersChanged)
    def my_servers(self):
        return self._model.my_servers

    # is_connected property
    @pyqtProperty('bool', notify=isConnectedChanged)
    def is_connected(self):
        return self._model.is_connected

    @pyqtSlot()
    def _on_is_connected_changed(self):
        self.status_text = 'Connected' if self.is_connected else 'Not connected'

    @pyqtSlot()
    def _on_is_connecting_changed(self):
        if self.is_connecting:
            self.status_text = 'Connecting'
        else:
            self._on_is_connected_changed()

    # is_connecting property
    @pyqtProperty('bool', notify=isConnectingChanged)
    def is_connecting(self):
        return self._model.is_connecting

    @pyqtProperty('QVariant', constant=True)
    def game_config(self):
        return QVariant(self._model.get_game_config())

    # status_text property
    @pyqtProperty('QString', notify=statusTextChanged)
    def status_text(self):
        return self._status_text

    @status_text.setter
    def status_text(self, status_text):
        self._status_text = status_text
        self.statusTextChanged.emit()

    # current_server property
    @pyqtProperty('QVariant', notify=currentServerChanged)
    def current_server(self):
        return QVariant(self._current_server)

    @current_server.setter
    def current_server(self, current_server):
        self._current_server = current_server
        self.currentServerChanged.emit()

    @pyqtSlot(str)
    def open_server(self, server_string):
        if server_string:
            try:
                self.status = False
                self.current_server = self._model.get_server_data(server_string)

                if self._current_server:
                    self.status = True
                else:
                    connect_string = self._model.get_connect_string(server_string)
                    if connect_string:
                        self.current_server = {
                            'connect_string': connect_string,
                            'host_name': 'Custom server'
                        }

                        self.status = True
                    else:
                        self.status = False
            except:
                self.status = False

    @pyqtSlot()
    def join_current_server(self):
        if self._current_server:
            try:
                self._model.play(self._current_server)
            except BaseException as e:
                QtWidgets.QMessageBox.critical(None, 'Error occured during launching quakelive', str(e))

    @pyqtSlot()
    def reconnect(self):
        self._model.reconnect()

    @pyqtSlot()
    def close_window(self):
        self._model.disconnect()
        exit(0)

    @pyqtSlot()
    def show_browser(self):
        print('ShowBrowser')

    @pyqtSlot()
    def show_friends(self):
        print('ShowFriends')

    @pyqtSlot()
    def launch_practice(self):
        self._model.launch_practice()

    @pyqtSlot(str, int)
    def update_live_streams(self, tag, limit):
        self.streams_is_updating = True

        @qtthreaddecorator.qt_thread_decorator()
        def _update_live_streams():
            self._model.live_streams = self._model.get_streams(tag, limit)
            self.streams_is_updating = False

        _update_live_streams()