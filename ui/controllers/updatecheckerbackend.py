"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


import logging
import os
from helpers import settings, optionsparser, qtthreaddecorator
import qllauncher as qll
from PyQt5.QtCore import pyqtProperty, QObject, pyqtSlot, pyqtSignal, QVariant, QTimer


class UpdateCheckerBackend(QObject):
    class UpdateState():
        Nothing = 0
        CheckingUpdate = 1
        NoGameFound = 2
        Updating = 3
        UpdateFinished = 4

    updateTextChanged = pyqtSignal()
    updateStateChanged = pyqtSignal()
    installGameTo = pyqtSignal(str)

    def __init__(self, parent=None):
        QObject.__init__(self)

        self._startup_update_check = not optionsparser.args.skip_update
        self._update_text = ''
        self._update_state = UpdateCheckerBackend.UpdateState.Nothing
        self.__need_update = False
        self.__timer = None
        self.__update_size = 0
        self.__update_date = None

        self.installGameTo.connect(self._download_and_install_new_game)

    # startup_update_check property
    @pyqtProperty('bool', constant=True)
    def startup_update_check(self):
        return self._startup_update_check

    # update_text property
    @pyqtProperty('QString', notify=updateTextChanged)
    def update_text(self):
        return self._update_text

    @update_text.setter
    def update_text(self, update_text):
        self._update_text = update_text
        logging.info(self._update_text)
        self.updateTextChanged.emit()

    # update_state property
    @pyqtProperty(int, notify=updateStateChanged)
    def update_state(self):
        return self._update_state

    @update_state.setter
    def update_state(self, update_state):
        if self._update_state != update_state:
            self._update_state = update_state
            self.updateStateChanged.emit()

    @pyqtSlot(str)
    def _download_and_install_new_game(self, directory):
        if directory:
            ql_base_path = directory.rstrip('/')
            _settings = settings.Settings()
            settings_dict = _settings.config_dict

            settings_dict['quake_live_base_path'] = ql_base_path
            settings_dict['quake_live_binary_path'] = '%s/quakelive.exe' % ql_base_path
            settings_dict['quake_live_home_path'] = '%s/home' % ql_base_path

            _settings.write_config(settings_dict)

            if not os.path.exists(os.path.dirname(settings_dict['quake_live_binary_path'])):
                os.makedirs(os.path.dirname(settings_dict['quake_live_binary_path']), exist_ok=True)

            if not os.path.exists(settings_dict['quake_live_base_path']):
                os.makedirs(settings_dict['quake_live_base_path'], exist_ok=True)

            if not os.path.exists(settings_dict['quake_live_home_path']):
                os.makedirs(settings_dict['quake_live_home_path'], exist_ok=True)

            self.check_update()

    @pyqtSlot()
    def check_update(self):
        self.update_state = UpdateCheckerBackend.UpdateState.CheckingUpdate
        _settings = settings.Settings()
        settings_dict = _settings.config_dict

        if _settings.is_blank or (not settings_dict['quake_live_binary_path'] or
            not settings_dict['quake_live_base_path'] or
            not settings_dict['quake_live_home_path'] or
            not os.path.exists(os.path.dirname(settings_dict['quake_live_binary_path'])) or
            not os.path.exists(settings_dict['quake_live_base_path']) or
            not os.path.exists(settings_dict['quake_live_home_path'])
        ):
            self.update_text = 'Specifying game directory'
            self.update_state = UpdateCheckerBackend.UpdateState.NoGameFound
        else:
            ql_handle = qll.QLHandle(settings_dict['quake_live_binary_path'],
                                     settings_dict['quake_live_home_path'],
                                     settings_dict['quake_live_base_path'])

            @qtthreaddecorator.qt_thread_decorator()
            def _check_update():
                ql_handle.extract_arenas_images(os.path.expanduser('~/.qllauncher/'))
                self.update_text = 'Retrieving latest update date...'
                self.__update_date = qll.QLHandle.check_update()
                self.update_text = 'Latest update date is "%s"' % self.__update_date

                if self.__update_date:
                    self.update_text = 'Checking game content...'
                    self.__update_size = ql_handle.get_update_size(self.__update_date)

                    if not self.__update_size:
                        self.update_text = 'No updated needed.'
                        self.update_state = UpdateCheckerBackend.UpdateState.Nothing

            @pyqtSlot()
            def _update_if_need():
                if self._update_state != UpdateCheckerBackend.UpdateState.Nothing:
                    self.update_state = UpdateCheckerBackend.UpdateState.Updating

                    @pyqtSlot()
                    def update_progress():
                        self.update_text = 'Downloading <strong>%s</strong> (%4.2f MB / %4.2f MB)' % \
                                           (ql_handle.downloading_file_name,
                                            float(ql_handle.downloaded_update_size / 1048576),
                                            float(self.__update_size / 1048576))

                    self.__timer = QTimer(self)
                    self.__timer.setInterval(1000)
                    self.__timer.timeout.connect(update_progress)
                    self.__timer.start()

                    @qtthreaddecorator.qt_thread_decorator()
                    def _update():
                        ql_handle.get_update_files(self.__update_date)

                    @pyqtSlot()
                    def _on_update_finished():
                        self.__timer.stop()
                        self.__timer = None
                        ql_handle.extract_arenas_images(os.path.expanduser('~/.qllauncher/'))
                        self.update_state = UpdateCheckerBackend.UpdateState.UpdateFinished

                    _update().finished.connect(_on_update_finished)

            _check_update().finished.connect(_update_if_need)
