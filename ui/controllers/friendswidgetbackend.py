"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


import qllauncher as qll
from PyQt5.QtCore import pyqtProperty, QObject, pyqtSlot, pyqtSignal, QVariant


class FriendsWidgetBackend(QObject, qll.QLXMPPFrontend, qll.NetworkStatusListener):
    friendsModelChanged = pyqtSignal()
    currentMessagesChanged = pyqtSignal()
    messageReceived = pyqtSignal()
    messageSent = pyqtSignal()
    hasNotificationsChanged = pyqtSignal()
    ownerCurrentlyOnServerChanged = pyqtSignal()
    messageCompanionChanged = pyqtSignal()

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        qll.QLXMPPFrontend.__init__(self)
        qll.NetworkStatusListener.__init__(self)

        self._friends_model = None
        self._owner_currently_on_server = None
        self._current_message_companion = None

    # quick_invite_list
    @pyqtProperty('QVariant', constant=True)
    def quick_invite_list(self):
        if self._xmpp_connection:
            _invite_list = {}
            for f in self._xmpp_connection.people_online:
                _invite_list[f['user']] = "Invite %s to %s" % (f['user'], self._owner_currently_on_server['ADDRESS'])

            return QVariant(_invite_list)

        return None

    # friends_model property
    @pyqtProperty('QVariant', notify=friendsModelChanged)
    def friends_model(self):
        if self._xmpp_connection:
            return QVariant(self._xmpp_connection.people_online)

        return None

    @friends_model.setter
    def friends_model(self, friends_model):
        self._friends_model = friends_model
        self.friendsModelChanged.emit()
        self.hasNotificationsChanged.emit()

    @pyqtProperty(bool, notify=hasNotificationsChanged)
    def has_notifications(self):
        if self._friends_model:
            for p in self._friends_model:
                if p['has_unread_messages']:
                    return True

        return False

    # current_messages property
    @pyqtProperty(str, notify=currentMessagesChanged)
    def current_messages(self):
        if self._current_message_companion:
            try:
                s = ''
                sorted_list = sorted(self._xmpp_connection.messages[self._current_message_companion['jid']])

                for key in sorted_list:
                    for item in self._xmpp_connection.messages[self._current_message_companion['jid']][key]:
                        s += '<b>%s</b> <i>%s</i>:<hr/>> %s<br/><br/>' % (item['from'].split('@')[0], key, item['message'])

                return s
            except BaseException:
                return ''

        return ''

    # owner_currently_on_server property
    @pyqtProperty('QVariant', notify=ownerCurrentlyOnServerChanged)
    def owner_currently_on_server(self):
        return QVariant(self._owner_currently_on_server)

    @owner_currently_on_server.setter
    def owner_currently_on_server(self, owner_currently_on_server):
        self._owner_currently_on_server = owner_currently_on_server
        self.ownerCurrentlyOnServerChanged.emit()

    # message_companion property
    @pyqtProperty('QVariant', notify=messageCompanionChanged)
    def message_companion(self):
        return QVariant(self._current_message_companion)

    @message_companion.setter
    def message_companion(self, message_companion):
        self._current_message_companion = message_companion
        self.currentMessagesChanged.emit()

        if message_companion:
            for guy in self._friends_model:
                if guy['jid'] == message_companion['jid'] and guy['has_unread_messages']:
                    guy['has_unread_messages'] = False
                    break

            self.friends_model = self._xmpp_connection.people_online

        self.messageCompanionChanged.emit()

    @pyqtSlot('QVariant', str)
    def send_message(self, to, message):
        self._xmpp_connection.send_message(to['jid'], message)
        self.currentMessagesChanged.emit()
        self.messageSent.emit()

    @pyqtSlot(str)
    def invite_user_to_current_server(self, user):
        if user and self._owner_currently_on_server:
            qll.QLNetwork().send_invite(user, self._owner_currently_on_server['SERVER_ID'])

    @pyqtSlot()
    def set_frontend(self):
        if qll.QLNetwork().is_connected():
            qll.QLNetwork().add_xmpp_frontend(self)
            self.friends_model = self._xmpp_connection.people_online

    def network_status_changed(self, status):
        if status == qll.QLNetwork.Status.CONNECTED:
            self.set_frontend()

    def event_received(self, event):
        if event.name == 'presence':
            self.friendsModelChanged.emit()

            if event.values['from'].jid == self._xmpp_connection.get_username() + '/quakelive':
                self.owner_currently_on_server = eval(event.values['status']) if event.values['status'] else None
        elif event.name == 'message':
            if self._current_message_companion and event.values['from'].jid != self._current_message_companion['jid']:
                try:
                    for guy in self._friends_model:
                        if guy['jid'] == event.values['from'].jid and not guy['has_unread_messages']:
                            guy['has_unread_messages'] = True
                            break

                    self.friends_model = self._xmpp_connection.people_online
                except KeyError:
                    pass

            self.messageReceived.emit()
            self.currentMessagesChanged.emit()
        # else:
            # self.unknown_event_received(event)