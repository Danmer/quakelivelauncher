"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


import logging
import collections
import json
from helpers import settings, qtthreaddecorator
from qllauncher.helpers import misc
import qllauncher as qll
from PyQt5.QtCore import pyqtProperty, QObject, pyqtSlot, pyqtSignal, QVariant


class ServerBrowserBackend(QObject, qll.NetworkStatusListener):
    class ServerUpdateState():
        Nothing = 0
        Updating = 1

    class SpawnStatus():
        Nothing = 0
        Spawned = 0
        Stopped = 0
        Spawning = 1
        Stopping = 2

    serversChanged = pyqtSignal()
    myServersChanged = pyqtSignal()
    serversUpdateStateChanged = pyqtSignal()
    filtersChanged = pyqtSignal()
    spawnParametersChanged = pyqtSignal()
    packedSpawnParametersChanged = pyqtSignal()
    spawnStatusChanged = pyqtSignal()
    spawnedServerLinksChanged = pyqtSignal()
    serverSpawnErrorChanged = pyqtSignal()
    subscriptionLevelChanged = pyqtSignal()
    spawnParametersHasErrorChanged = pyqtSignal()

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        qll.NetworkStatusListener.__init__(self)

        self._servers = []
        self.__servers_qvariant = None
        self._servers_update_state = ServerBrowserBackend.ServerUpdateState.Nothing
        self._filter = json.loads(settings.Settings().config_dict['filter'])
        self._spawn_parameters = {}
        self._packed_spawn_parameters = ''
        self._spawned_server_links = {}
        self._my_servers = []
        self._spawn_status = ServerBrowserBackend.SpawnStatus.Nothing
        self._spawn_error = None
        self._subscription_level = 'standard'
        self._spawn_parameters_has_error = False

    @pyqtProperty('QVariant', constant=True)
    def quick_spawn_server_list(self):
        for location in qll.QLNetwork.locations:
            pass

    @pyqtProperty(bool, notify=spawnParametersHasErrorChanged)
    def spawn_parameters_has_error(self):
        return self._spawn_parameters_has_error

    @spawn_parameters_has_error.setter
    def spawn_parameters_has_error(self, spawn_parameters_has_error):
        self._spawn_parameters_has_error = spawn_parameters_has_error
        self.spawnParametersHasErrorChanged.emit()

    @pyqtProperty(str, notify=subscriptionLevelChanged)
    def subscription_level(self):
        return self._subscription_level

    @subscription_level.setter
    def subscription_level(self, subscription_level):
        self._subscription_level = subscription_level
        self.subscriptionLevelChanged.emit()

    @pyqtProperty('QVariant', notify=serverSpawnErrorChanged)
    def spawn_error(self):
        return QVariant(self._spawn_error)

    @spawn_error.setter
    def spawn_error(self, spawn_error):
        self._spawn_error = spawn_error
        self.serverSpawnErrorChanged.emit()
        logging.error("Can't spawn a server: " + spawn_error)

    @pyqtProperty('QVariant', notify=spawnedServerLinksChanged)
    def spawned_server_links(self):
        return QVariant(self._spawned_server_links)

    @spawned_server_links.setter
    def spawned_server_links(self, spawned_server_links):
        self._spawned_server_links = spawned_server_links
        self.spawnedServerLinksChanged.emit()

    @pyqtProperty(int, notify=spawnStatusChanged)
    def spawn_status(self):
        return self._spawn_status

    @spawn_status.setter
    def spawn_status(self, spawn_status):
        self._spawn_status = spawn_status
        self.spawnStatusChanged.emit()

    @pyqtProperty('QVariant', notify=serversChanged)
    def servers(self):
        return self.__servers_qvariant

    @servers.setter
    def servers(self, servers):
        self._servers = servers
        self.__servers_qvariant = QVariant(self._servers)
        self.serversChanged.emit()

    @pyqtProperty('QVariant', notify=myServersChanged)
    def my_servers(self):
        return QVariant(self._my_servers)

    @my_servers.setter
    def my_servers(self, my_servers):
        self._my_servers = my_servers
        self.myServersChanged.emit()
        logging.debug('servers got: %s' % str(my_servers))

    @pyqtProperty(str, notify=packedSpawnParametersChanged)
    def packed_spawn_parameters(self):
        return self._packed_spawn_parameters

    @packed_spawn_parameters.setter
    def packed_spawn_parameters(self, packed_spawn_parameters):
        self._packed_spawn_parameters = packed_spawn_parameters
        self.packedSpawnParametersChanged.emit()

    @pyqtSlot()
    def update_packed_spawn_parameters(self):
        try:
            self.packed_spawn_parameters = str(self._get_packed_spawn_parameters())
        except:
            pass

    @servers.setter
    def servers(self, servers):
        self._servers = servers
        self.__servers_qvariant = QVariant(self._servers)
        self.serversChanged.emit()

    @pyqtProperty(int, notify=serversUpdateStateChanged)
    def servers_update_state(self):
        return self._servers_update_state

    @servers_update_state.setter
    def servers_update_state(self, servers_update_state):
        self._servers_update_state = servers_update_state
        self.serversUpdateStateChanged.emit()

    @pyqtProperty('QVariant', notify=filtersChanged)
    def filters(self):
        return QVariant(self._pack_filters())

    @filters.setter
    def filters(self, new_filters):
        self._filter = self._unpack_filters(new_filters)
        self.filtersChanged.emit()

    @pyqtProperty('QVariant', notify=spawnParametersChanged)
    def spawn_parameters(self):
        return QVariant(self._spawn_parameters)

    @spawn_parameters.setter
    def spawn_parameters(self, spawn_parameters):
        self._spawn_parameters = spawn_parameters
        self.spawnParametersChanged.emit()

    def _get_packed_spawn_parameters(self):
        location = misc.dict_get_key_by_value(qll.QLNetwork.locations,
                                              self._spawn_parameters['location']['selected'])
        game_type = misc.dict_get_key_by_value(qll.QLNetwork.game_types,
                                               self._spawn_parameters['game_type']['selected'])
        rule_set = misc.dict_get_key_by_value(qll.QLNetwork.rule_sets,
                                              self._spawn_parameters['ruleset']['selected'])
        map = [self._spawn_parameters['map']['selected']]
        host_name = self._spawn_parameters['host_name']
        server_password = self._spawn_parameters['server_password']
        invites = self._spawn_parameters['invites'].split(',')

        packed_settings = qll.QLNetwork.get_packed_server_settings(location,
                                                                   game_type,
                                                                   map,
                                                                   server_password,
                                                                   host_name,
                                                                   rule_set,
                                                                   invites)

        return packed_settings

    def network_status_changed(self, status):
        if status == qll.QLNetwork.Status.CONNECTED:
            self.fill_filters()
            self.fill_spawn_parameters()
            self.update_packed_spawn_parameters()
            self.subscription_level = qll.QLNetwork().ql_user_info['LEVEL']

    @pyqtSlot()
    def fill_filters(self):
        self.filters = self._pack_filters()

    @pyqtSlot()
    def fill_spawn_parameters(self):
        spawn_parameters = {
            'location': {
                'values': '',
                'selected': ''
            },

            'ruleset': {
                'values': '',
                'selected': ''
            },

            'map': {
                'values': '',
                'selected': ''
            },

            'game_type': {
                'values': '',
                'selected': ''
            },
            'invites': '',
            'host_name': '',
            'server_password': '',
            'packed_settings': ''
        }

        def _fill_locations():
            locations_values = []
            sorted_locations = sorted(qll.QLNetwork.locations.values())
            for item in sorted_locations:
                if item != qll.QLNetwork.locations['any'] and item != qll.QLNetwork.locations['ALL']:
                    locations_values.append(item)

            spawn_parameters['location']['values'] = locations_values
            spawn_parameters['location']['selected'] = locations_values[0]

        def _fill_rule_sets():
            rulesets_values = []
            for item in qll.QLNetwork.rule_sets.values():
                rulesets_values.append(item)

            spawn_parameters['ruleset']['values'] = rulesets_values
            spawn_parameters['ruleset']['selected'] = rulesets_values[0]

        def _fill_maps():
            spawn_parameters['map']['values'] = qll.QLNetwork().ql_map_db

        def _fill_game_types():
            game_type_values = []
            for item in qll.QLNetwork.game_types.values():
                if item != qll.QLNetwork.game_types['any']:
                    game_type_values.append(item)

            spawn_parameters['game_type']['values'] = game_type_values
            spawn_parameters['game_type']['selected'] = game_type_values[0]

        _fill_locations()
        _fill_rule_sets()
        _fill_game_types()
        _fill_maps()

        self.spawn_parameters = spawn_parameters

    @pyqtSlot(str)
    def parse_new_packed_parameters(self, packed_settings_str):
        try:
            eval(packed_settings_str)
            self.packed_spawn_parameters = packed_settings_str
            self.spawn_parameters_has_error = False
        except SyntaxError:
            self.spawn_parameters_has_error = True

    @pyqtSlot()
    def update_my_servers(self):
        @qtthreaddecorator.qt_thread_decorator()
        def _get_my_servers():
            self.my_servers = qll.QLNetwork().get_spawned_servers_list()

        if self._subscription_level == 'pro':
            _get_my_servers()

    @pyqtSlot()
    def update_servers(self):
        self.servers_update_state = ServerBrowserBackend.ServerUpdateState.Updating
        ql_network = qll.QLNetwork()

        if ql_network.is_connected():

            @qtthreaddecorator.qt_thread_decorator()
            def _get_servers():
                servers = ql_network.get_server_list(self._filter)['servers']
                readable_servers = []

                for server in servers:
                    try:
                        owner = server['owner']
                    except KeyError:
                        owner = 'Id Software'

                    players = qll.QLNetwork.parse_players_count(server)

                    try:
                        readable_server = {
                            'location': qll.QLNetwork.locations[server['location_id']].split('-')[1].strip(),
                            'flag': qll.QLNetwork.location_flags[server['location_id']],
                            'name': server['host_name'],
                            'owner': owner,
                            'map': server['map'],
                            'players': '%d/%d' % (players[0], players[2]),
                            'ip': server['host_address'],
                            'protected': 'Yes' if server['g_needpass'] else 'No',
                            'game_type': qll.QLNetwork.game_types[server['game_type']],
                            'ruleset': server['ruleset'],
                            'link': qll.QLNetwork._quake_live_join_link_pattern + str(server['public_id'])
                        }

                        readable_servers.append(readable_server)
                    except KeyError as e:
                        logging.error("Can't get server list: %s" % str(e))
                    except BaseException as e:
                        logging.critical("CRITICAL: " + str(e))

                self.servers = readable_servers
                self.servers_update_state = ServerBrowserBackend.ServerUpdateState.Nothing

            _get_servers()


    @pyqtSlot()
    def reset_filter(self):
        self._filter = qll.ServerBrowser.create_server_browser_filter()
        self.fill_filters()

    @pyqtSlot()
    def save_filter(self):
        _settings = settings.Settings()
        config_dict = _settings.config_dict
        config_dict['filter'] = json.dumps(self._filter, sort_keys=True, indent=2, ensure_ascii=True)
        _settings.write_config(config_dict)

    def _unpack_filters(self, filters):
        game_group = misc.dict_get_key_by_value(qll.ServerBrowser.FILTERS_GROUP, filters['gamePlayers']['selected'])
        game_type = misc.dict_get_key_by_value(qll.QLNetwork.game_types, filters['gameType']['selected'])
        game_state = misc.dict_get_key_by_value(qll.ServerBrowser.FILTERS_STATE, filters['gameState']['selected'])
        game_difficulty = misc.dict_get_key_by_value(qll.ServerBrowser.FILTERS_DIFFICULTY, filters['gameDifficulty']['selected'])
        game_location = misc.dict_get_key_by_value(qll.QLNetwork.locations, filters['gameLocation']['selected'])
        game_private = misc.dict_get_key_by_value(qll.ServerBrowser.FILTERS_PRIVATE, filters['gameMatches']['selected'])

        return qll.ServerBrowser.create_server_browser_filter(group=game_group,
                                                              game_type=game_type,
                                                              state=game_state,
                                                              difficulty=game_difficulty,
                                                              location=game_location,
                                                              private=game_private)

    def _pack_filters(self):
        ordered_game_types = collections.OrderedDict(qll.QLNetwork.game_types.items())
        ordered_game_states = collections.OrderedDict(qll.ServerBrowser.FILTERS_STATE.items())
        ordered_game_difficulty = collections.OrderedDict(qll.ServerBrowser.FILTERS_DIFFICULTY.items())
        ordered_game_matches = collections.OrderedDict(qll.ServerBrowser.FILTERS_PRIVATE.items())

        _game_locations = [qll.QLNetwork.locations['any'], qll.QLNetwork.locations['ALL']]
        _other_game_locations = qll.QLNetwork.locations.copy()
        _other_game_locations.pop('ALL')
        _other_game_locations.pop('any')

        for loc_value in sorted(_other_game_locations.values()):
            _game_locations.append(loc_value)

        if self._filter['filters']['location'] not in qll.QLNetwork.locations.keys():
            self._filter['filters']['location'] = 'any'

        filters = {
            'gamePlayers': {
                'values': sorted(qll.ServerBrowser.FILTERS_GROUP.values()),
                'selected': qll.ServerBrowser.FILTERS_GROUP[self._filter['filters']['group']]
            },

            'gameType': {
                'values': list(ordered_game_types.values()),
                'selected': qll.QLNetwork.game_types[self._filter['filters']['game_type']]
            },

            'gameState': {
                'values': sorted(ordered_game_states.values()),
                'selected': qll.ServerBrowser.FILTERS_STATE[self._filter['filters']['state']]
            },

            'gameDifficulty': {
                'values': list(ordered_game_difficulty.values()),
                'selected': qll.ServerBrowser.FILTERS_DIFFICULTY[self._filter['filters']['difficulty']]
            },

            'gameLocation': {
                'values': _game_locations,
                'selected': qll.QLNetwork.locations[self._filter['filters']['location']]
            },

            'gameMatches': {
                'values': list(ordered_game_matches.values()),
                'selected': qll.ServerBrowser.FILTERS_PRIVATE[self._filter['filters']['private']]
            }
        }

        return filters

    @pyqtSlot()
    def spawn_server(self):
        packed_spawn_parameters = eval(self._packed_spawn_parameters)
        self.spawn_status = ServerBrowserBackend.SpawnStatus.Spawning

        @qtthreaddecorator.qt_thread_decorator()
        def _spawn_in_thread():
            self.spawned_server_links = qll.QLNetwork().spawn_server_with_settings(packed_spawn_parameters)

        @pyqtSlot(str)
        def _on_spawning_finished(error):
            if not error:
                self.spawn_status = ServerBrowserBackend.SpawnStatus.Spawned
                self.update_my_servers()
            else:
                self.spawn_status = ServerBrowserBackend.SpawnStatus.Nothing
                self.spawn_error = error

        _spawn_in_thread().finished_signal.connect(_on_spawning_finished)

    @pyqtSlot(str)
    def stop_server(self, server_id):
        self.spawn_status = ServerBrowserBackend.SpawnStatus.Stopping

        @qtthreaddecorator.qt_thread_decorator()
        def _stop_server_in_thread():
            qll.QLNetwork().send_stop_server_and_wait_until_it_really_stop(server_id)
            self.spawn_status = ServerBrowserBackend.SpawnStatus.Stopped
            self.update_my_servers()

        _stop_server_in_thread()