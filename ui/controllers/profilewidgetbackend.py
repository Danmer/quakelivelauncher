"""
QuakeLive Launcher
Copyright (C) 2013  Victor Polevoy (vityatheboss@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'

import qllauncher as qll
from helpers import qtthreaddecorator
from PyQt5.QtCore import pyqtProperty, QObject, pyqtSlot, pyqtSignal, QVariant


class ProfileWidgetBackend(QObject):
    targetProfileChanged = pyqtSignal()
    profileFetchStatusChanged = pyqtSignal()
    profileDataChanged = pyqtSignal()
    profileRequestTypeChanged = pyqtSignal()
    profileEloStatsChanged = pyqtSignal()

    class ProfileFetchStatus():
        Nothing = 0
        Fetching = 1
        NotExists = 2
        Done = 3
        Error = 4

    def __init__(self, parent=None):
        QObject.__init__(self, parent)

        self._profile_request_type = qll.QLProfileRequestType.SUMMARY
        self._profile_data = None
        self._profile_elo_stats = None
        self._target_profile = None
        self._profile_fetch_status = ProfileWidgetBackend.ProfileFetchStatus.Nothing
        self.targetProfileChanged.connect(self._update_profile_information)

    # profile_fetch_status property
    @pyqtProperty(int, notify=profileFetchStatusChanged)
    def profile_fetch_status(self):
        return self._profile_fetch_status

    @profile_fetch_status.setter
    def profile_fetch_status(self, profile_fetch_status):
        self._profile_fetch_status = profile_fetch_status
        self.profileFetchStatusChanged.emit()

    # profile_data property
    @pyqtProperty('QVariant', notify=profileDataChanged)
    def profile_data(self):
        return QVariant(self._profile_data)

    @profile_data.setter
    def profile_data(self, profile_data):
        self._profile_data = profile_data
        self.profileDataChanged.emit()

    # profile_request_type property
    @pyqtProperty(str, notify=profileRequestTypeChanged)
    def profile_request_type(self):
        return self._profile_request_type

    @profile_request_type.setter
    def profile_request_type(self, profile_request_type):
        self._profile_request_type = profile_request_type
        self.profileRequestTypeChanged.emit()

    # profile_elo_stats property
    @pyqtProperty('QVariant', notify=profileEloStatsChanged)
    def profile_elo_stats(self):
        return QVariant(self._profile_elo_stats)

    @profile_elo_stats.setter
    def profile_elo_stats(self, profile_elo_stats):
        self._profile_elo_stats = profile_elo_stats
        self.profileEloStatsChanged.emit()

    # target_profile property
    @pyqtProperty(str, notify=targetProfileChanged)
    def target_profile(self):
        return self._target_profile

    @target_profile.setter
    def target_profile(self, target_profile):
        self._target_profile = target_profile
        self.targetProfileChanged.emit()

    @pyqtSlot()
    def _update_profile_information(self):
        @qtthreaddecorator.qt_thread_decorator()
        def _get_profile_information():
            self.profile_fetch_status = ProfileWidgetBackend.ProfileFetchStatus.Fetching
            if qll.QLProfile.is_player_exists(self._target_profile):
                self.profile_data = qll.QLProfile(self._target_profile, self._profile_request_type, True).data
                self.profile_elo_stats = qll.QLProfile(self._target_profile, self._profile_request_type).get_elo()
                self.profile_fetch_status = ProfileWidgetBackend.ProfileFetchStatus.Done
            else:
                self.profile_fetch_status = ProfileWidgetBackend.ProfileFetchStatus.NotExists
                self.profile_data = None

        _get_profile_information()