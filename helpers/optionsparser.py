__author__ = 'Victor Polevoy'


import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="Verbose mode", dest='verbosity', action="store_true")
parser.add_argument("-i", "--info", help="Information", dest='information', action="store_true")
parser.add_argument("--skip-update", help="Skip update at startup", dest='skip_update', action="store_true")
args = parser.parse_args()