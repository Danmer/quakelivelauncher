"""
QuakeLive Launcher
Copyright (C) 2014  Victor Polevoy <vityatheboss@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Victor Polevoy'


import configparser
import os
import json

from qllauncher import serverbrowser


class Settings():
    _home_app_path = '/.qllauncher/'

    _blank_settings_dict = {
        'quake_live_binary_path': '',
        'quake_live_home_path': '',
        'quake_live_base_path': '',
        'email': '',
        'password': '',
        'taskset': '',
        'wineskin_path': '',
        'sound_muted': False,
        'sound_volume': 5,
        'filter': json.dumps(serverbrowser.ServerBrowser.blank_filter, sort_keys=True, indent=2, ensure_ascii=True)
    }

    def __init__(self):
        self._config_parser = configparser.RawConfigParser()
        self._settings_file_name = os.path.expanduser("~") + Settings._home_app_path + 'settings.conf'
        self.config_dict = {}
        self.is_blank = False
        self.read_config()

    def _correct_missed_keys(self):
        keys = Settings._blank_settings_dict.keys()

        for key in keys:
            if key not in self.config_dict.keys():
                self.config_dict[key] = Settings._blank_settings_dict[key]

    def read_config(self):
        if os.path.exists(self._settings_file_name):
            self._config_parser.read(self._settings_file_name)
            for section in self._config_parser.sections():
                self.config_dict = {}
                for key, val in self._config_parser.items(section):
                    self.config_dict[key] = val

            self._correct_missed_keys()
        else:
            self.config_dict = Settings._blank_settings_dict
            self.is_blank = True

    def write_config(self, dictionary):
        if not os.path.exists(os.path.expanduser("~") + Settings._home_app_path):
            os.makedirs(os.path.expanduser("~") + Settings._home_app_path)

        with open(self._settings_file_name, 'w+') as f:
            settings_with_dummy_section = {
                'settings': dictionary
            }
            self._config_parser.read_dict(settings_with_dummy_section)
            self._config_parser.write(f)