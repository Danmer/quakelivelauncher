__author__ = 'Victor Polevoy'


from PyQt5 import QtCore


class Worker(QtCore.QThread):
    threads = []    # we have to store at least one reference to any thread;otherwise they'll be garbage-collected
    finished_signal = QtCore.pyqtSignal(object)

    def __init__(self, thread_name, function, *args, **kwargs):
        QtCore.QThread.__init__(self)

        self._thread_name = thread_name
        self._function = function
        self._args = args
        self._kwargs = kwargs

    def run(self):
        Worker.threads.append(self.currentThreadId())
        status = None
        try:
            status = self._function(*self._args, **self._kwargs)
        except Exception as e:
            status = str(e)

        try:
            self.finished_signal.emit(status)
        except:
            pass

        try:
            Worker.threads.remove(self.currentThreadId())
        except:
            pass

    def __del__(self):
        self.wait()


def qt_thread_decorator():
    def decorator(function):
        def wrapper(*args, **kwargs):
            worker = Worker(function.__name__, function, *args, **kwargs)

            def on_finish():
                try:
                    worker.quit()
                except NameError:
                    pass

            worker.finished.connect(on_finish)
            worker.start()

            return worker
        return wrapper
    return decorator