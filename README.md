## QuakeLive Launcher by FX
QuakeLive Launcher by FX - is a native standalone application to launch QuakeLive on Linux.
The launcher uses [qllauncher](https://bitbucket.org/fx_/python3-qllauncher/overview) python library as backend.

[Donate](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=DVHN3DL3WRTSC&lc=RU&item_name=Victor%20Polevoy%20%28QLLauncher%29&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted)


## Requirements
Any debian-based distro

Python 3 ( >= 3.2.3 )

Qt5 ( >= 5.1 )

For more information see [Dependencies](DEPENDENCIES.md)

## Installation
Through ppa (Personal Package Archives):

```bash
sudo add-apt-repository ppa:broken/ppa
sudo apt-get update
sudo apt-get install qllauncher
```

## Issues
If you have any issues please do the following:

1. Take information about the modules by launching `qllauncher -i`.
2. Try to reproduce your error with running `qllauncher -v` and take this information too.
3. Use collected information in previous steps as epilog during issue posting.

[JIRA Issues page](https://jaibot.atlassian.net/secure/RapidBoard.jspa?rapidView=2&view=planning)

[BitBucket Issues Page](https://bitbucket.org/fx_/quakelivelauncher/issues)

## News && Release Notes
[Blog page](http://blog.vpolevoy.com/python/tag/qllauncher/)

## License
Licensed under GPL v3 license

Copyright (c) 2014 Victor Polevoy
