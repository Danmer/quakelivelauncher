# Common dependencies:
* qt5-default
* python3-pyqt5
* python3-pyqt5.qtquick
* python3-requests ( >= 2.4.3 (pip))
* python3-sleekxmpp ( >= 1.3.1 (pip) or >=  1.0~beta5-2 (python3-sleekxmpp deb package))
* python3-qllauncher
* wine
* x11-xserver-utils (for mouse control through ***xset***)
* python3-opengl (not really necessary but debian packages of pyqt5 have a bug which can be solved by using this)
* fx-qml-controls-plugin
* fx-qml-helpers-plugin
* libqt5svg5

# Ubuntu/Mint dependencies:
* qtdeclarative5-window-plugin
* qtdeclarative5-qtmultimedia-plugin
* qtdeclarative5-dialogs-plugin
* qtdeclarative5-quicklayouts-plugin
* qtdeclarative5-qtquick2-plugin
* qtdeclarative5-controls-plugin
* libqt5qml-graphicaleffects
* libqt5multimedia5

# Debian dependencies:
* qml-module-qtquick-window2
* qml-module-qtmultimedia
* qml-module-qtquick-dialogs
* qml-module-qtquick-layouts
* qml-module-qtquick2
* qml-module-qtquick-controls
* qml-module-qtgraphicaleffects
* libqt5multimedia5-plugins

## What is this
If you want to run the launcher right from the repository then you should have installed all the dependencies.

* If you try to run launcher on *ubuntu* then your dependencies are ***Common*** + ***Ubuntu/Mint***.
* If you try to run launcher on *debian* then your dependencies are ***Common*** + ***Debian***.